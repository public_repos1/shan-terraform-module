provider "aws" {
  alias  = "devops"
  region = "us-east-1"
}

provider "aws" {
  alias  = "mgmt"
  region = "us-east-1"
  assume_role {
    role_arn     = "arn:aws:iam::588214404713:role/GitlabAccountAccessRole"
    session_name = "GitlabRunner_DevOps"
  }
}

provider "aws" {
  alias  = "source"
  region = "us-east-1"
  assume_role {
    role_arn     = var.source_aws_role_arn
    session_name = "GitlabRunner_DevOps"
  }
}

provider "aws" {
  region = var.aws_region
  assume_role {
    role_arn     = var.aws_role_arn
    session_name = "terraform"
    external_id  = var.aws_external_id
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.main.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.main.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.main.token
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.main.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.main.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.main.token
  }
  debug = true
}
