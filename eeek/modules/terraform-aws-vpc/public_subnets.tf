resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.main.id
  count                   = var.deploy_public_subnets ? length(local.subnet_list[0]) : 0
  cidr_block              = local.subnet_list[0][count.index]
  availability_zone       = element(data.aws_availability_zones.available.names, count.index + var.skip_az)
  map_public_ip_on_launch = true

  tags = merge({
    Name        = "${var.name}-public-${element(split("-", element(data.aws_availability_zones.available.names, count.index + var.skip_az)), length(split("-", element(data.aws_availability_zones.available.names, count.index + var.skip_az))) - 1)}"
    Environment = local.environment
  }, var.tags, var.public_subnet_tags)
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
  count  = var.deploy_public_subnets ? 1 : 0
  tags = merge({
    Name        = "${var.name}-public"
    Environment = local.environment
  }, var.tags)
}

resource "aws_route" "public" {
  count                  = var.deploy_public_subnets ? length(local.subnet_list[0]) : 0
  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_route_table_association" "public" {
  count          = var.deploy_public_subnets ? length(local.subnet_list[0]) : 0
  subnet_id      = element(aws_subnet.public[*].id, count.index)
  route_table_id = aws_route_table.public[0].id
}
