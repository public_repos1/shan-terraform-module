resource "aws_subnet" "private" {
  vpc_id                  = aws_vpc.main.id
  count                   = var.deploy_private_subnets ? length(local.subnet_list[1]) : 0
  cidr_block              = local.subnet_list[1][count.index]
  availability_zone       = element(data.aws_availability_zones.available.names, count.index + var.skip_az)
  map_public_ip_on_launch = false

  tags = merge({
    Name        = "${var.name}-private-${element(split("-", element(data.aws_availability_zones.available.names, count.index + var.skip_az)), length(split("-", element(data.aws_availability_zones.available.names, count.index + var.skip_az))) - 1)}"
    Environment = local.environment
  }, var.tags, var.private_subnet_tags)
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
  count  = var.deploy_private_subnets ? length(local.subnet_list[1]) : 0

  tags = merge({
    Name        = "${var.name}-private-${element(split("-", element(data.aws_availability_zones.available.names, count.index + var.skip_az)), length(split("-", element(data.aws_availability_zones.available.names, count.index + var.skip_az))) - 1)}"
    Environment = local.environment
  }, var.tags)
}

resource "aws_route" "private" {
  count                  = var.deploy_private_subnets ? length(local.subnet_list[1]) : 0
  route_table_id         = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.main.*.id, count.index)
}

resource "aws_route_table_association" "private" {
  count          = var.deploy_private_subnets ? length(local.subnet_list[1]) : 0
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}
