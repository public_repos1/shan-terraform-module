variable "name" {
  type    = string
  default = null
}

variable "full_bucket_name" {
  type    = string
  default = null
}

variable "environment" {
  type    = string
  default = null
}

variable "versioning_enabled" {
  type    = bool
  default = false
}

variable "s3_bucket_acl" {
  description = "The canned ACL to apply. Valid values are private, public-read, public-read-write, aws-exec-read, authenticated-read, and log-delivery-write. Defaults to private."
  type        = string
  default     = "private"
}

variable "sse_algorithm" {
  type    = string
  default = "AES256"
}

variable "kms_master_key_id" {
  default = null
}

variable "tags" {
  default = {}
}
