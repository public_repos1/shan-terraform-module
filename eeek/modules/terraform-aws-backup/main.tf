resource "aws_backup_plan" "main" {
  name = "data_backup_plan"

  rule {
    rule_name                = "s3_backup"
    target_vault_name        = aws_backup_vault.main.name
    schedule                 = "cron(0 9 * * ? *)"
    completion_window        = 5760
    enable_continuous_backup = false
    lifecycle {
      cold_storage_after = 30
    }
    recovery_point_tags = {
      Environment = var.environment
    }
  }
  tags = {
    Environment = var.environment
  }
}

resource "aws_backup_selection" "main" {
  iam_role_arn = aws_iam_role.backup.arn
  name         = "backup_selection"
  plan_id      = aws_backup_plan.main.id

  selection_tag {
    type  = "STRINGEQUALS"
    key   = "aws_backup"
    value = "True"
  }
}

resource "aws_backup_vault" "main" {
  name = "backup_vault"
  tags = {
    Environment = var.environment
  }
}

resource "aws_backup_vault_policy" "main" {
  backup_vault_name = aws_backup_vault.main.name

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "default",
  "Statement": [
    {
      "Sid": "default",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "backup:DescribeBackupVault",
        "backup:DeleteBackupVault",
        "backup:PutBackupVaultAccessPolicy",
        "backup:DeleteBackupVaultAccessPolicy",
        "backup:GetBackupVaultAccessPolicy",
        "backup:StartBackupJob",
        "backup:GetBackupVaultNotifications",
        "backup:PutBackupVaultNotifications"
      ],
      "Resource": "${aws_backup_vault.main.arn}"
    }
  ]
}
POLICY
}
