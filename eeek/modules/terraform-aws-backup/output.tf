output "id" {
  value = aws_backup_plan.main.id
}

output "arn" {
  value = aws_backup_plan.main.arn
}
