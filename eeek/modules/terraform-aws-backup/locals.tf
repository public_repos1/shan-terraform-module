locals {
  name        = var.name
  tags        = var.tags
  environment = var.environment == null ? terraform.workspace : var.environment
}