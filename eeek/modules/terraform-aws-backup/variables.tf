variable "name" {
  default = "unnamed"
}

variable "environment" {
  type    = string
  default = null
}

variable "tags" {
  default = {}
}
