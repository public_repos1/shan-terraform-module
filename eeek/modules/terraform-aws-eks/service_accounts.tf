resource "kubernetes_service_account" "harmonize-gitlab" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "harmonize-gitlab"
    namespace = "kubeflow"
  }
  depends_on = [
    kubernetes_namespace.kubeflow
  ]
}

resource "kubernetes_service_account" "dapr-gitlab" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "dapr-gitlab"
    namespace = "dapr-system"
  }
  depends_on = [
    kubernetes_namespace.dapr-system
  ]
}

resource "kubernetes_service_account" "temporal-gitlab" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "temporal-gitlab"
    namespace = "temporal"
  }
  depends_on = [
    kubernetes_namespace.temporal
  ]
}

resource "kubernetes_service_account" "gitlab" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "gitlab"
    namespace = "kube-system"
  }
}
