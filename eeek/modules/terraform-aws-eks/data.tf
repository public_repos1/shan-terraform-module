data "aws_availability_zones" "available" {}
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
data "aws_partition" "current" {}

data "aws_ami" "eks_worker_gpu" {
  filter {
    name   = "name"
    values = ["amazon-eks-gpu-node-${var.eks_version}-*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  most_recent = true
  owners      = ["602401143452"] //amazon
}
