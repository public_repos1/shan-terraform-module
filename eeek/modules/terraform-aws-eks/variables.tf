variable "name" {
  description = "Name of the EKS cluster"
  type        = string
  default     = "eeek"
}

variable "environment" {
  description = "Environment Name, defaults to workspace name"
  type        = string
  default     = null
}

variable "eks_version" {
  description = "Cluster Version"
  type        = string
  default     = "1.19"
}

variable "node_groups" {
  description = "Node Group Managed"
}

variable "ssh_key_name" {
  type = string
}

variable "vpc_id" {
  description = "EKS Cluster's VPC"
  type        = string
}

variable "subnet_ids" {
  description = "EKS Cluster's Subnets"
  type        = list(string)
}

variable "source_security_group_ids" {
  description = "source sg for worker nodes ssh"
  type        = list(string)
  default     = []
}

variable "endpoint_private_access" {
  description = "EKS Private Access Enabled"
  type        = bool
  default     = true
}

variable "endpoint_public_access" {
  description = "EKS Public Access Enabled"
  type        = bool
  default     = false
}

variable "endpoint_public_access_cidrs" {
  description = "EKS Public Access list"
  type        = list(string)
  default     = null
}

variable "enabled_cluster_log_types" {
  default     = []
  description = "enabled control plane logging"
  type        = list(string)
}

variable "cluster_log_retention_in_days" {
  default     = 14
  description = "Number of days to retain log events"
  type        = number
}

variable "key_usage" {
  default     = "ENCRYPT_DECRYPT"
  description = "KMS Key Usage"
  type        = string
}

variable "key_spec" {
  default     = "SYMMETRIC_DEFAULT"
  description = "KMS Specification"
  type        = string
}

variable "deletion_window" {
  default     = 10
  description = "key deletion windows in days"
  type        = number
}

variable "is_enabled" {
  default     = true
  description = "key is enabled"
  type        = bool
}

variable "enable_key_rotation" {
  default     = true
  description = "key rotation enabled"
  type        = bool
}

variable "service_ipv4_cidr" {
  description = "service ipv4 cidr"
  type        = string
  default     = null
}

variable "eks_oidc_root_ca_thumbprint" {
  type        = string
  description = "Thumbprint of Root CA for EKS OIDC, Valid until 2037"
  default     = "9e99a48a9960b14926bb7f3b02e22da2b0ab7280"
}

variable "create_additional_rbac_roles" {
  description = "additional rbac rules"
  type        = bool
  default     = true
}

variable "timeouts" {
  description = "Timeout for creating and destroying the EKS cluster"
  type        = map(string)
  default = {
    create = "30m"
    delete = "15m"
  }
}

variable "tags" {
  description = "Tags"
  type        = map(string)
  default     = {}
}

variable "cluster_security_group_tags" {
  type    = map(string)
  default = {}
}

variable "worker_security_group_tags" {
  type    = map(string)
  default = {}
}
