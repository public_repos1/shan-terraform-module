##############################
# KMS Key for EKS CloudWatch #
##############################
resource "aws_kms_alias" "cloudwatch" {
  count         = length(var.enabled_cluster_log_types) > 0 ? 1 : 0
  name          = "alias/eks/logs/${var.name}"
  target_key_id = aws_kms_key.cloudwatch[0].key_id
}

resource "aws_kms_key" "cloudwatch" {
  count                    = length(var.enabled_cluster_log_types) > 0 ? 1 : 0
  description              = "/eks/logs/${var.name}"
  key_usage                = var.key_usage
  customer_master_key_spec = var.key_spec
  deletion_window_in_days  = var.deletion_window
  is_enabled               = var.is_enabled
  enable_key_rotation      = var.enable_key_rotation

  tags = local.tags

  policy = data.aws_iam_policy_document.kms_logs[0].json
}

data "aws_iam_policy_document" "kms_logs" {
  count = length(var.enabled_cluster_log_types) > 0 ? 1 : 0
  statement {
    sid       = "Enable IAM User Permissions"
    effect    = "Allow"
    actions   = ["kms:*"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
  statement {
    effect    = "Allow"
    actions   = ["kms:Encrypt*", "kms:Decrypt*", "kms:ReEncrypt*", "kms:GenerateDataKey*", "kms:Describe*"]
    resources = ["*"]
    principals {
      type        = "Service"
      identifiers = ["logs.${data.aws_region.current.name}.amazonaws.com"]
    }
    condition {
      test     = "ArnEquals"
      variable = "kms:EncryptionContext:aws:logs:arn"
      values   = ["arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/eks/${var.name}/cluster"]
    }
  }
}

###########################
# KMS Key for EKS Secrets #
###########################
resource "aws_kms_alias" "secrets" {
  name          = "alias/eks/secrets/${var.name}"
  target_key_id = aws_kms_key.secrets.key_id
}

resource "aws_kms_key" "secrets" {
  description              = "/eks/logs/${var.name}"
  key_usage                = var.key_usage
  customer_master_key_spec = var.key_spec
  deletion_window_in_days  = var.deletion_window
  is_enabled               = var.is_enabled
  enable_key_rotation      = var.enable_key_rotation

  tags = local.tags

  policy = data.aws_iam_policy_document.kms_secrets.json
}

data "aws_iam_policy_document" "kms_secrets" {
  statement {
    sid       = "Enable IAM User Permissions"
    effect    = "Allow"
    actions   = ["kms:*"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}
