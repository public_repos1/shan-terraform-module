############################
# IAM Role for EKS Cluster #
############################
resource "aws_iam_role" "cluster" {
  name_prefix = "eks-${var.name}-cluster-"
  tags        = local.tags
  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "eks.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster.name
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.cluster.name
}

############################
# IAM Role for EKS Workers #
############################
resource "aws_iam_role" "workers" {
  name_prefix = "eks-${var.name}-workers-"
  tags        = local.tags
  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_instance_profile" "workers" {
  name = aws_iam_role.workers.name
  role = aws_iam_role.workers.name
}

resource "aws_iam_role_policy_attachment" "workers_AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.workers.name
}

resource "aws_iam_role_policy_attachment" "workers_AmazonEKSCNIPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.workers.name
}

resource "aws_iam_role_policy_attachment" "workers_CloudWatchAgentServerPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  role       = aws_iam_role.workers.name
}

resource "aws_iam_role_policy_attachment" "workers_AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.workers.name
}

resource "aws_iam_role_policy_attachment" "workers_AmazonSSMManagedInstanceCore" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.workers.name
}

resource "aws_iam_policy" "workers_ecr" {
  name_prefix = "AmazonECRAccess_"
  path        = "/"
  description = "IAM Policy for ECR Full Access"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action   = ["ecr:*", "cloudtrail:LookupEvents"]
      Effect   = "Allow"
      Resource = "*"
      }, {
      Action   = ["iam:CreateServiceLinkedRole"]
      Effect   = "Allow"
      Resource = "*"
      Condition = {
        StringEquals = {
          "iam:AWSServiceName" = ["replication.ecr.amazonaws.com"]
        }
      }
    }]
  })
}

resource "aws_iam_role_policy_attachment" "workers_ecr" {
  policy_arn = aws_iam_policy.workers_ecr.arn
  role       = aws_iam_role.workers.name
}