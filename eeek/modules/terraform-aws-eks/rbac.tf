resource "kubernetes_cluster_role" "aggregate-dapr-component-admin" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "aggregate-dapr-component-admin"
    labels = {
      "rbac.authorization.k8s.io/aggregate-to-admin" = "true"
    }
  }

  rule {
    api_groups = ["dapr.io"]
    resources  = ["components"]
    verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"]
  }
}

resource "kubernetes_role" "harmonize-port-forward" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "harmonize-port-forward"
    namespace = "kubeflow"
  }

  rule {
    api_groups = [""]
    resources  = ["services"]
    verbs      = ["get"]
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list"]
  }

  rule {
    api_groups = [""]
    resources  = ["pods/portforward"]
    verbs      = ["create"]
  }

  depends_on = [
    kubernetes_namespace.kubeflow
  ]
}

resource "kubernetes_role_binding" "harmonize-port-forward" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "harmonize-port-forward"
    namespace = "kubeflow"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "harmonize-port-forward"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "harmonize-gitlab"
    namespace = "kubeflow"
  }

  depends_on = [
    kubernetes_namespace.kubeflow
  ]
}

resource "kubernetes_role_binding" "dapr-gitlab-edit-dapr-system" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "dapr-gitlab-edit-dapr-system"
    namespace = "dapr-system"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "edit"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "dapr-gitlab"
    namespace = "dapr-system"
  }

  depends_on = [
    kubernetes_namespace.dapr-system
  ]
}

resource "kubernetes_role" "dapr-gitlab-crds" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "dapr-gitlab-crds"
    namespace = "dapr-system"
  }

  rule {
    api_groups = ["dapr.io"]
    resources  = ["configurations", "components", "subscriptions"]
    verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"]
  }

  depends_on = [
    kubernetes_namespace.dapr-system
  ]
}

resource "kubernetes_role_binding" "dapr-gitlab-crds" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "dapr-gitlab-crds"
    namespace = "dapr-system"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "dapr-gitlab-crds"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "dapr-gitlab"
    namespace = "dapr-system"
  }

  depends_on = [
    kubernetes_namespace.dapr-system
  ]
}

resource "kubernetes_cluster_role" "dapr-gitlab-cluster-permissions" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "dapr-gitlab-cluster-permissions"
  }

  rule {
    api_groups = [
      ""
    ]
    resources = [
      "namespaces"
    ]
    verbs = [
      "get",
      "list",
      "watch"
    ]
  }

  rule {
    api_groups = [
      "admissionregistration.k8s.io",
    ]
    resources = [
      "mutatingwebhookconfigurations",
    ]
    verbs = [
      "get",
      "list",
      "watch",
      "create",
      "update",
      "patch",
      "delete",
    ]
  }

  rule {
    api_groups = [
      "rbac.authorization.k8s.io",
    ]
    resources = [
      "clusterroles",
      "clusterrolebindings",
      "roles",
      "rolebindings",
    ]
    verbs = [
      "get",
      "list",
      "watch",
      "create",
      "update",
      "patch",
      "delete",
    ]
  }

  rule {
    api_groups = [
      "",
    ]
    resources = [
      "namespaces",
    ]
    verbs = [
      "get",
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = [
      "admissionregistration.k8s.io",
    ]
    resources = [
      "mutatingwebhookconfigurations",
    ]
    verbs = [
      "get",
      "list",
      "watch",
      "create",
      "update",
      "patch",
      "delete",
    ]
  }

  rule {
    api_groups = [
      "rbac.authorization.k8s.io",
    ]
    resources = [
      "clusterroles",
      "clusterrolebindings",
      "roles",
      "rolebindings",
    ]
    verbs = [
      "get",
      "list",
      "watch",
      "create",
      "update",
      "patch",
      "delete",
    ]
  }

  rule {
    api_groups = [
      "*",
    ]
    resources = [
      "serviceaccounts",
      "deployments",
      "services",
      "statefulsets",
      "configmaps",
      "secrets",
      "components",
      "configurations",
      "subscriptions",
      "leases",
    ]
    verbs = [
      "get",
    ]
  }

  rule {
    api_groups = [
      "*",
    ]
    resources = [
      "deployments",
      "services",
      "statefulsets",
      "components",
      "configurations",
      "subscriptions",
      "leases",
      "secrets",
    ]
    verbs = [
      "list",
      "watch",
    ]
  }

  rule {
    api_groups = [
      "*",
    ]
    resources = [
      "services",
      "secrets",
      "statefulsets",
      "subscriptions",
      "configmaps",
      "leases",
      "services/finalizers",
      "statefulsets/finalizers",
      "deployments/finalizers",
    ]
    verbs = [
      "update",
    ]
  }

  rule {
    api_groups = [
      "*",
    ]
    resources = [
      "customresourcedefinitions",
    ]
    verbs = [
      "get",
      "list",
      "watch",
      "create",
      "update",
      "patch",
      "delete",
    ]
  }

  rule {
    api_groups = [
      "*",
    ]
    resources = [
      "services",
      "leases",
    ]
    verbs = [
      "delete",
    ]
  }

  rule {
    api_groups = [
      "*",
    ]
    resources = [
      "deployments",
      "services",
      "statefulsets",
      "configmaps",
      "events",
      "leases",
    ]
    verbs = [
      "create",
    ]
  }

  rule {
    api_groups = [
      "",
    ]
    resources = [
      "pods",
      "pods/log",
    ]
    verbs = [
      "get",
      "list",
    ]
  }

  rule {
    api_groups = [
      "apps",
    ]
    resources = [
      "pods",
      "pods/log",
      "namespaces",
    ]
    verbs = [
      "get",
      "list",
    ]
  }

  rule {
    api_groups = [
      "dapr.io",
    ]
    resources = [
      "pods",
      "pods/log",
      "namespaces",
    ]
    verbs = [
      "get",
      "list",
    ]
  }

  rule {
    api_groups = [
      "extensions",
    ]
    resources = [
      "pods",
      "pods/log",
      "namespaces",
    ]
    verbs = [
      "get",
      "list",
    ]
  }

  rule {
    api_groups = [
      "authentication.k8s.io",
    ]
    resources = [
      "tokenreviews",
    ]
    verbs = [
      "create",
    ]
  }

  rule {
    api_groups = [
      "authorization.k8s.io",
    ]
    resources = [
      "subjectaccessreviews",
    ]
    verbs = [
      "create",
    ]
  }
}

resource "kubernetes_cluster_role_binding" "dapr-gitlab-cluster-permissions" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "dapr-gitlab-cluster-permissions"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "dapr-gitlab-cluster-permissions"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "dapr-gitlab"
    namespace = "dapr-system"
  }
}

resource "kubernetes_role_binding" "temporal-gitlab-edit-temporal" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name      = "temporal-gitlab-edit-temporal"
    namespace = "temporal"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "edit"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "temporal-gitlab"
    namespace = "temporal"
  }

  depends_on = [
    kubernetes_namespace.temporal
  ]
}

resource "kubernetes_cluster_role" "temporal-gitlab-cluster-permissions" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "temporal-gitlab-cluster-permissions"
  }

  rule {
    api_groups = [""]
    resources  = ["namespaces"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "temporal-gitlab-cluster-permissions" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "temporal-gitlab-cluster-permissions"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "temporal-gitlab-cluster-permissions"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "temporal-gitlab"
    namespace = "temporal"
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-admin" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "gitlab-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "gitlab"
    namespace = "kube-system"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "gitlab"
  }
}
