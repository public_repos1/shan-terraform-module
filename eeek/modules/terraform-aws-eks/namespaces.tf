resource "kubernetes_namespace" "kubeflow" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "kubeflow"
    labels = {
      "control-plane"                    = "kubeflow"
      "istio-injection"                  = "enabled"
      "katib-metricscollector-injection" = "enabled"
    }
  }
}

resource "kubernetes_namespace" "dapr-system" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "dapr-system"
  }
}

resource "kubernetes_namespace" "temporal" {
  count = var.create_additional_rbac_roles ? 1 : 0
  metadata {
    name = "temporal"
  }
}
