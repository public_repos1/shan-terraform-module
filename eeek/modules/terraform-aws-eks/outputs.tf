output "eks" {
  value = aws_eks_cluster.main
}

output "workers_security_group_id" {
  value = aws_security_group.workers.id
}

output "cluster_security_group_id" {
  value = aws_security_group.cluster.id
}

output "worker_iam_role_name" {
  value = aws_iam_role.workers.name
}

output "worker_instance_profile" {
  value = aws_iam_instance_profile.workers.name
}
