locals {
  environment = var.environment == null ? terraform.workspace : var.environment
  tags = merge({
    Name        = var.name
    Environment = local.environment
  }, var.tags)
}

resource "aws_cloudwatch_log_group" "main" {
  count             = length(var.enabled_cluster_log_types) > 0 ? 1 : 0
  name              = "/aws/eks/${var.name}/cluster"
  retention_in_days = var.cluster_log_retention_in_days
  kms_key_id        = aws_kms_key.cloudwatch[0].arn
  tags              = local.tags
}

resource "aws_eks_cluster" "main" {
  name                      = var.name
  enabled_cluster_log_types = var.enabled_cluster_log_types
  role_arn                  = aws_iam_role.cluster.arn
  version                   = var.eks_version
  tags                      = local.tags

  vpc_config {
    security_group_ids      = tolist([aws_security_group.cluster.id])
    subnet_ids              = var.subnet_ids
    endpoint_private_access = var.endpoint_private_access
    endpoint_public_access  = var.endpoint_public_access
    public_access_cidrs     = var.endpoint_public_access_cidrs
  }

  kubernetes_network_config {
    service_ipv4_cidr = var.service_ipv4_cidr
  }

  timeouts {
    create = var.timeouts["create"]
    delete = var.timeouts["delete"]
  }

  encryption_config {
    provider {
      key_arn = aws_kms_key.secrets.arn
    }
    resources = ["secrets"]
  }

  depends_on = []

  lifecycle {
    create_before_destroy = false
  }
}

resource "aws_eks_node_group" "main" {
  for_each        = var.node_groups
  cluster_name    = aws_eks_cluster.main.name
  node_group_name = each.key
  node_role_arn   = aws_iam_role.workers.arn

  subnet_ids    = lookup(each.value, "subnet_ids", var.subnet_ids)
  ami_type      = lookup(each.value, "ami_type", "AL2_x86_64")
  capacity_type = lookup(each.value, "capacity_type", "ON_DEMAND")
  disk_size     = lookup(each.value, "disk_size", 100)

  force_update_version = false

  version = var.eks_version

  tags = merge({ for node_group_label_name, node_group_label_value in lookup(each.value, "labels", {}) :
    "k8s.io/cluster-autoscaler/node-template/label/${node_group_label_name}" => node_group_label_value
    }, { for taint in lookup(each.value, "taints", {}) :
    "k8s.io/cluster-autoscaler/node-template/taint/${taint["key"]}" => "${taint["value"]}:${taint["effect"]}"
  }, local.tags, lookup(each.value, "tags", {}))

  instance_types = lookup(each.value, "instance_types", ["m5a.large"])

  scaling_config {
    desired_size = lookup(each.value, "desired_size", 1)
    max_size     = lookup(each.value, "max_size", 1)
    min_size     = lookup(each.value, "min_size", 1)
  }

  labels = lookup(each.value, "labels", {})

  dynamic "taint" {
    for_each = lookup(each.value, "taints", [])

    content {
      key    = taint.value["key"]
      value  = taint.value["value"]
      effect = taint.value["effect"]
    }
  }

  #  remote_access {
  #    ec2_ssh_key               = var.ssh_key_name
  #    source_security_group_ids = var.source_security_group_ids
  #  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.workers_AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.workers_AmazonEKSCNIPolicy,
    aws_iam_role_policy_attachment.workers_CloudWatchAgentServerPolicy,
    aws_iam_role_policy_attachment.workers_AmazonSSMManagedInstanceCore,
    aws_iam_role_policy_attachment.workers_AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.workers_ecr
  ]

  lifecycle {
    create_before_destroy = false
    ignore_changes        = [scaling_config.0.desired_size]
  }
}

resource "aws_iam_openid_connect_provider" "main" {
  client_id_list  = ["sts.${data.aws_partition.current.dns_suffix}"]
  thumbprint_list = [var.eks_oidc_root_ca_thumbprint]
  url             = aws_eks_cluster.main.identity[0].oidc.0.issuer
}
