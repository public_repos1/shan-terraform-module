resource "aws_s3_bucket" "main" {
  bucket_prefix = "${local.name}-"
  tags          = local.tags
}

resource "aws_s3_bucket_acl" "main" {
  bucket = aws_s3_bucket.main.id
  acl    = "private"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "main" {
  bucket = aws_s3_bucket.main.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = var.sse_algorithm
      kms_master_key_id = var.kms_master_key_id
    }
    bucket_key_enabled = true
  }
}

resource "aws_s3_bucket_versioning" "main" {
  bucket = aws_s3_bucket.main.id
  versioning_configuration {
    status = var.versioning_enabled ? "Enabled" : "Suspended"
  }
}

resource "aws_s3_object" "main" {
  bucket      = aws_s3_bucket.main.id
  key         = "requirements.txt"
  source      = "${path.module}/files/requirements.txt"
  source_hash = filemd5("${path.module}/files/requirements.txt")
  kms_key_id  = var.kms_master_key_id
}


resource "aws_s3_bucket_public_access_block" "main" {
  bucket                  = aws_s3_bucket.main.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_policy" "main" {
  bucket     = aws_s3_bucket.main.id
  depends_on = [aws_s3_bucket_public_access_block.main]
  policy     = <<POLICY
{
  "Id": "sslPolicy",
  "Version": "2012-10-17",
  "Statement": [{
    "Sid": "AllowSSLRequestsOnly",
    "Action": "s3:*",
    "Effect": "Deny",
    "Resource": [
      "${aws_s3_bucket.main.arn}",
      "${aws_s3_bucket.main.arn}/*"
    ],
    "Condition": {
      "Bool": {
        "aws:SecureTransport":"false"
      }
    },
    "Principal":"*"
  }, {
    "Sid":"Prevent bucket delete",
    "Effect":"Deny",
    "Principal":"*",
    "Action":"s3:DeleteBucket",
    "Resource":[
      "${aws_s3_bucket.main.arn}",
      "${aws_s3_bucket.main.arn}/*"
    ]
  }]
}
POLICY
}

resource "aws_mwaa_environment" "main" {
  name                           = local.name
  environment_class              = var.environment_class
  dag_s3_path                    = var.dag_s3_path
  requirements_s3_path           = var.requirements_s3_path
  requirements_s3_object_version = var.versioning_enabled ? aws_s3_object.main.version_id : null
  execution_role_arn             = aws_iam_role.main.arn
  airflow_version                = var.airflow_version
  min_workers                    = var.min_workers
  max_workers                    = var.max_workers

  airflow_configuration_options = {
    "celery.sync_parallelism"             = 1
    "scheduler.min_file_process_interval" = 120
    "scheduler.dag_dir_list_interval"     = 600
    "core.dagbag_import_timeout"          = 120
    "core.dag_file_processor_timeout"     = 150
  }

  logging_configuration {
    dag_processing_logs {
      enabled   = local.logs.dag_processing.enabled
      log_level = local.logs.dag_processing.level
    }

    scheduler_logs {
      enabled   = local.logs.scheduler.enabled
      log_level = local.logs.scheduler.level
    }

    task_logs {
      enabled   = local.logs.task.enabled
      log_level = local.logs.task.level
    }

    webserver_logs {
      enabled   = local.logs.webserver.enabled
      log_level = local.logs.webserver.level
    }

    worker_logs {
      enabled   = local.logs.worker.enabled
      log_level = local.logs.worker.level
    }
  }

  network_configuration {
    security_group_ids = [aws_security_group.main.id]
    subnet_ids         = var.subnet_ids
  }

  source_bucket_arn               = aws_s3_bucket.main.arn
  weekly_maintenance_window_start = "MON:17:30"

  depends_on = [aws_iam_policy.main, aws_iam_role_policy_attachment.main]

}
