output "airflow" {
  value = aws_mwaa_environment.main
}

output "security_group_id" {
  value = aws_security_group.main.id
}
