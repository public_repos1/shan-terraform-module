data "aws_iam_policy_document" "main" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["airflow.amazonaws.com", "airflow-env.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "main" {
  name_prefix        = "${var.name}-"
  tags               = var.tags
  assume_role_policy = data.aws_iam_policy_document.main.json
}

resource "aws_iam_policy" "main" {
  name_prefix = "${var.name}-"
  path        = "/"
  description = "IAM Policy to be used by Apache Managed Airflow ${var.name}"

  policy = <<JSON
{
  "Version": "2012-10-17",
  "Statement": [{
    "Effect": "Allow",
    "Action": "airflow:PublishMetrics",
    "Resource": "arn:aws:airflow:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:environment/${var.name}"
  }, {
    "Effect": "Deny",
    "Action": "s3:ListAllMyBuckets",
    "Resource": [
      "${aws_s3_bucket.main.arn}",
      "${aws_s3_bucket.main.arn}/*"
    ]
  }, {
    "Effect": "Allow",
    "Action": [
      "s3:GetObject*",
      "s3:PutObject",
      "s3:GetBucket*",
      "s3:List*"
    ],
    "Resource": [
      "${aws_s3_bucket.main.arn}",
      "${aws_s3_bucket.main.arn}/*"
    ]
  }, {
    "Effect": "Allow",
    "Action": [
      "logs:CreateLogStream",
      "logs:CreateLogGroup",
      "logs:PutLogEvents",
      "logs:GetLogEvents",
      "logs:GetLogRecord",
      "logs:GetLogGroupFields",
      "logs:GetQueryResults"
    ],
    "Resource": [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:airflow-${var.name}-*"
    ]
  }, {
    "Effect": "Allow",
    "Action": [
      "logs:DescribeLogGroups"
    ],
    "Resource": [
      "*"
    ]
  }, {
    "Effect": "Allow",
    "Action": "cloudwatch:PutMetricData",
    "Resource": "*"
  }, {
    "Effect": "Allow",
    "Action": [
      "sqs:ChangeMessageVisibility",
      "sqs:DeleteMessage",
      "sqs:GetQueueAttributes",
      "sqs:GetQueueUrl",
      "sqs:ReceiveMessage",
      "sqs:SendMessage"
    ],
    "Resource": "arn:aws:sqs:${data.aws_region.current.name}:*:airflow-celery-*"
  }, {
    "Effect": "Allow",
    "Action": [
      "kms:Decrypt",
      "kms:DescribeKey",
      "kms:GenerateDataKey*",
      "kms:Encrypt"
    ],
    "NotResource": "arn:aws:kms:*:${data.aws_caller_identity.current.account_id}:key/*",
    "Condition": {
      "StringLike": {
        "kms:ViaService": [
          "sqs.${data.aws_region.current.name}.amazonaws.com",
          "s3.${data.aws_region.current.name}.amazonaws.com"
        ]
      }
    }
  }]
}
JSON
}

resource "aws_iam_role_policy_attachment" "main" {
  policy_arn = aws_iam_policy.main.arn
  role       = aws_iam_role.main.name
}
