locals {
  name        = var.name
  tags        = var.tags
  logs        = merge(local.logs_default, var.logs)
  environment = var.environment == null ? terraform.workspace : var.environment
}
