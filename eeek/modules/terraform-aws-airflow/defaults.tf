locals {
  logs_default = {
    dag_processing = {
      enabled = true
      level   = "INFO"
    }
    scheduler = {
      enabled = true
      level   = "INFO"
    }
    task = {
      enabled = true
      level   = "INFO"
    }
    webserver = {
      enabled = true
      level   = "INFO"
    }
    worker = {
      enabled = true
      level   = "INFO"
    }
  }
}
