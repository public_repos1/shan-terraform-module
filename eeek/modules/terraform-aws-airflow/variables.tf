variable "name" {
  default = "unnamed"
}

variable "environment" {
  type    = string
  default = null
}

variable "environment_class" {
  default = "mw1.large"
}

variable "security_group_ids" {
  type    = list(string)
  default = []
}

variable "ingress_security_group_rules" {
  type    = map(any)
  default = {}
}

variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "dag_s3_path" {
  default = "dags/"
}

variable "requirements_s3_path" {
  default = "requirements.txt"
}

variable "logs" {
  type    = map(any)
  default = {}
}

variable "versioning_enabled" {
  type    = bool
  default = true
}

variable "airflow_version" {
  type    = string
  default = "2.0.2"
}

variable "min_workers" {
  type    = string
  default = "2"
}

variable "max_workers" {
  type    = string
  default = "2"
}

variable "s3_bucket_acl" {
  description = "The canned ACL to apply. Valid values are private, public-read, public-read-write, aws-exec-read, authenticated-read, and log-delivery-write. Defaults to private."
  type        = string
  default     = "private"
}

variable "sse_algorithm" {
  type    = string
  default = "AES256"
}

variable "kms_master_key_id" {
  default = null
}

variable "tags" {
  default = {}
}
