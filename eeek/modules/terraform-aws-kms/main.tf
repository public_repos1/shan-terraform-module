resource "aws_kms_alias" "main" {
  name          = "alias${var.name}"
  target_key_id = aws_kms_key.main.key_id
}

resource "aws_kms_key" "main" {
  description              = var.name
  key_usage                = var.key_usage
  customer_master_key_spec = var.key_spec
  deletion_window_in_days  = var.deletion_window
  is_enabled               = var.is_enabled
  enable_key_rotation      = var.enable_key_rotation
  tags                     = var.tags
  policy                   = data.aws_iam_policy_document.main.json
}

data "aws_iam_policy_document" "main" {
  statement {
    sid       = "Enable IAM User Permissions"
    effect    = "Allow"
    actions   = ["kms:*"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}
