variable "name" {
  description = "Name"
  type        = string
}

variable "description" {
  description = "Description"
  type        = string
  default     = null
}

variable "key_usage" {
  default     = "ENCRYPT_DECRYPT"
  description = "KMS Key Usage"
  type        = string
}

variable "key_spec" {
  default     = "SYMMETRIC_DEFAULT"
  description = "KMS Specification"
  type        = string
}

variable "deletion_window" {
  default     = 10
  description = "key deletion windows in days"
  type        = number
}

variable "is_enabled" {
  default     = true
  description = "key is enabled"
  type        = bool
}

variable "enable_key_rotation" {
  default     = true
  description = "key rotation enabled"
  type        = bool
}

variable "tags" {
  description = "Tags"
  type        = map(string)
  default     = {}
}
