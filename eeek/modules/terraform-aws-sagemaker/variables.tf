variable "name" {
  default = "unnamed"
}

variable "sagemaker_domain" {
  type    = string
  default = "default"
}

variable "environment" {
  type    = string
  default = null
}

variable "security_group_ids" {
  type    = list(string)
  default = []
}

variable "ingress_security_group_rules" {
  type    = map(any)
  default = {}
}

variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "tags" {
  description = "Tags"
  type        = map(string)
  default     = {}
}

# KMS Settings
variable "kms_master_key_id" {
  default = null
}

variable "key_usage" {
  default     = "ENCRYPT_DECRYPT"
  description = "KMS Key Usage"
  type        = string
}

variable "key_spec" {
  default     = "SYMMETRIC_DEFAULT"
  description = "KMS Specification"
  type        = string
}

variable "deletion_window" {
  default     = 10
  description = "key deletion windows in days"
  type        = number
}

variable "is_enabled" {
  default     = true
  description = "key is enabled"
  type        = bool
}

variable "enable_key_rotation" {
  default     = false
  description = "key rotation enabled"
  type        = bool
}

variable "s3_output_path" {
  default = null
}
