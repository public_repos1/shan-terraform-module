resource "aws_kms_alias" "main" {
  count         = var.kms_master_key_id == null ? 1 : 0
  name          = "alias/sagemaker/${var.name}"
  target_key_id = aws_kms_key.main[0].key_id
}

resource "aws_kms_key" "main" {
  count                    = var.kms_master_key_id == null ? 1 : 0
  description              = "For SageMaker EFS and EBS data"
  key_usage                = var.key_usage
  customer_master_key_spec = var.key_spec
  deletion_window_in_days  = var.deletion_window
  is_enabled               = var.is_enabled
  enable_key_rotation      = var.enable_key_rotation

  tags = local.tags

  policy = data.aws_iam_policy_document.main[0].json
}

data "aws_iam_policy_document" "main" {
  count = var.kms_master_key_id == null ? 1 : 0
  statement {
    sid       = "Enable IAM User Permissions"
    effect    = "Allow"
    actions   = ["kms:*"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
  statement {
    sid       = "Allow use of the key"
    effect    = "Allow"
    actions   = ["kms:Encrypt*", "kms:Decrypt*", "kms:ReEncrypt*", "kms:GenerateDataKey*", "kms:Describe*"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AmazonSageMakerExecutionRole"]
    }
  }
  statement {
    sid       = "Allow attachment of persistent resources"
    effect    = "Allow"
    actions   = ["kms:CreateGrant", "kms:ListGrants", "kms:RevokeGrant"]
    resources = ["*"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AmazonSageMakerExecutionRole"]
    }
    condition {
      test     = "Bool"
      variable = "kms:GrantIsForAWSResource"
      values   = ["true"]
    }
  }
}
