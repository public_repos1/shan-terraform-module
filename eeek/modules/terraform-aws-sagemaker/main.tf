resource "aws_sagemaker_domain" "main" {
  domain_name             = var.sagemaker_domain
  auth_mode               = "IAM"
  vpc_id                  = var.vpc_id
  subnet_ids              = var.subnet_ids
  app_network_access_type = "VpcOnly"
  kms_key_id              = aws_kms_key.main[0].arn
  default_user_settings {
    execution_role  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AmazonSageMakerExecutionRole"
    security_groups = ["sg-012d4809d27146414"]
    sharing_settings {
      notebook_output_option = "Allowed"
      s3_kms_key_id          = aws_kms_key.main[0].arn
      s3_output_path         = var.s3_output_path
    }
  }
}
