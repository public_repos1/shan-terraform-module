locals {
  environment = var.environment == null ? terraform.workspace : var.environment
}

resource "aws_iam_role" "main" {
  name_prefix = "${var.name}-"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_instance_profile" "main" {
  name = aws_iam_role.main.name
  role = aws_iam_role.main.name
}

resource "aws_iam_role_policy_attachment" "ProvisionerAmazonSSMManagedInstanceCore" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.main.name
}

resource "aws_instance" "main" {
  ami                    = var.ami_id == null ? local.ami_id : var.ami_id
  instance_type          = var.instance_type
  key_name               = var.ssh_key_name
  vpc_security_group_ids = concat(tolist([aws_security_group.main.id]), var.security_group_ids)
  subnet_id              = var.subnet_id
  iam_instance_profile   = aws_iam_instance_profile.main.name

  root_block_device {
    delete_on_termination = var.root_volume["delete_on_termination"]
    encrypted             = var.root_volume["encrypted"]
    throughput            = var.root_volume["throughput"]
    iops                  = var.root_volume["iops"]
    kms_key_id            = var.root_volume["kms_key_id"]
    volume_size           = var.root_volume["volume_size"]
    volume_type           = var.root_volume["volume_type"]
  }

  user_data         = var.user_data
  get_password_data = var.get_password_data

  volume_tags = merge({
    Name        = var.name
    Environment = local.environment
  }, var.tags)

  tags = merge({
    Name        = var.name
    Environment = local.environment
  }, var.tags)

  lifecycle {
    ignore_changes = all
  }
}

resource "aws_eip" "main" {
  count = var.eip ? 1 : 0
  vpc   = true
  tags = merge({
    Name        = var.name
    Environment = local.environment
  }, var.tags)
}

resource "aws_eip_association" "main" {
  count         = var.eip ? 1 : 0
  instance_id   = aws_instance.main.id
  allocation_id = aws_eip.main[0].id
}
