variable "name" {
  type    = string
  default = "provisioner"
}

variable "environment" {
  type    = string
  default = null
}

variable "eip" {
  type    = bool
  default = false
}

variable "ssh_key_name" {
  type = string
}

variable "ami_id" {
  type        = string
  default     = null
  description = "pass ami_id to override default images"
}

variable "linux_distro" {
  type        = string
  default     = "amazon"
  description = "either amazon or ubuntu when deploying default images. This is ignored when passing ami_id"
}

variable "instance_type" {
  type        = string
  default     = "t3.medium"
  description = "instance type"
}

variable "root_volume" {
  type = map(any)
  default = {
    delete_on_termination = true
    encrypted             = true
    throughput            = 125
    iops                  = 3000
    kms_key_id            = null
    volume_size           = 30
    volume_type           = "gp3"
  }
  description = "volume details"
}

variable "user_data" {
  default = null
}

variable "get_password_data" {
  description = "valid only on windows instances"
  default     = null
}

variable "security_group_ids" {
  type    = list(string)
  default = []
}

variable "ingress_security_group_rules" {
  type    = map(any)
  default = {}
}

variable "vpc_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "tags" {
  default = {}
}
