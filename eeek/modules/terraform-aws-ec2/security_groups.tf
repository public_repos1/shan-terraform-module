resource "aws_security_group" "main" {
  name_prefix = var.name
  description = var.name
  vpc_id      = var.vpc_id

  tags = merge({
    Name        = var.name
    Environment = local.environment
  }, var.tags)
}

resource "aws_security_group_rule" "main_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.main.id
}

resource "aws_security_group_rule" "main_ingress_self" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  self              = true
  security_group_id = aws_security_group.main.id
}

resource "aws_security_group_rule" "main_ingress" {
  for_each                 = var.ingress_security_group_rules
  type                     = "ingress"
  security_group_id        = aws_security_group.main.id
  description              = each.key
  from_port                = each.value["from_port"]
  to_port                  = each.value["to_port"]
  protocol                 = lookup(each.value, "protocol", "tcp")
  cidr_blocks              = length(compact(split(",", lookup(each.value, "cidr_blocks", "")))) == 0 ? null : split(",", lookup(each.value, "cidr_blocks", ""))
  source_security_group_id = lookup(each.value, "source_security_group_id", null)
}
