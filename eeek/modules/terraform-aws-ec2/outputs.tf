output "id" {
  value = aws_instance.main.id
}

output "private_ip" {
  value = aws_instance.main.private_ip
}

output "private_cidr" {
  value = "${aws_instance.main.private_ip}/32"
}

output "eip" {
  value = aws_eip.main[*].public_ip
}

output "sg_id" {
  value = aws_security_group.main.id
}

output "password_data" {
  value = aws_instance.main.password_data
}
