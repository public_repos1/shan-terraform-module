locals {
  oidc_provider = replace(data.aws_eks_cluster.main.identity[0].oidc[0].issuer, "https://", "")
  account_id    = data.aws_caller_identity.current.account_id
}

resource "aws_iam_role" "main" {
  name_prefix = "eks-${var.cluster_name}-irsa-${var.name}-"
  description = var.description
  tags        = var.tags
  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRoleWithWebIdentity"
      Effect = "Allow"
      Principal = {
        Federated = "arn:aws:iam::${local.account_id}:oidc-provider/${local.oidc_provider}"
      }
      Condition = {
        StringEquals = {
          "${local.oidc_provider}:sub" = "system:serviceaccount:${var.namespace}:${var.service_account}"
        }
      }
    }]
    Version = "2012-10-17"
  })
}

resource "kubernetes_namespace_v1" "main" {
  count = var.create_namespace ? 1 : 0

  metadata {
    name = var.namespace
  }

  lifecycle {
    ignore_changes = all
  }
}

resource "kubernetes_service_account" "main" {
  count = var.create_service_account ? 1 : 0
  metadata {
    name      = var.service_account
    namespace = var.namespace
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.main.arn
    }
    labels = var.labels
  }
  automount_service_account_token = true

  lifecycle {
    ignore_changes = all
  }

  depends_on = [
    kubernetes_namespace_v1.main
  ]
}

resource "kubernetes_annotations" "main" {
  count       = var.annotate_service_account ? 1 : 0
  api_version = "v1"
  kind        = "ServiceAccount"

  metadata {
    name      = var.service_account
    namespace = var.namespace
  }

  annotations = {
    "eks.amazonaws.com/role-arn" = aws_iam_role.main.arn
  }

  depends_on = [
    kubernetes_service_account.main
  ]
}
