output "service_account" {
  value = kubernetes_service_account.main
}

output "iam_role_arn" {
  value = aws_iam_role.main.arn
}

output "iam_role_name" {
  value = aws_iam_role.main.name
}
