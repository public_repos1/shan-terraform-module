variable "name" {
  description = "Name"
  type        = string
}

variable "description" {
  description = "Description"
  type        = string
  default     = null
}

variable "namespace" {
  description = "Kubernetes Namespace"
  type        = string
}

variable "cluster_name" {
  description = "eks cluster_name / id"
  type        = string
}

variable "service_account" {
  description = "Kubernetes Service Account Name"
  type        = string
}

variable "create_service_account" {
  type    = bool
  default = false
}

variable "annotate_service_account" {
  type    = bool
  default = false
}

variable "create_namespace" {
  type    = bool
  default = false
}

variable "labels" {
  type    = map(string)
  default = {}
}

variable "tags" {
  type    = map(string)
  default = {}
}
