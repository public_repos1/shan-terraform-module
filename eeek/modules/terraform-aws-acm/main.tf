resource "aws_acm_certificate" "main" {
  domain_name               = var.domain_name
  subject_alternative_names = var.subject_alternative_names
  validation_method         = "DNS"
  options {
    certificate_transparency_logging_preference = "ENABLED"
  }
  tags = var.tags

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "main" {
  name         = var.dns_zone
  private_zone = false
}

locals {
  domain_names = distinct(
    [for domain_name in concat([var.domain_name], var.subject_alternative_names) : replace(domain_name, "*.", "")]
  )
  validation_domains = [for k, v in aws_acm_certificate.main.domain_validation_options : tomap(v) if contains(local.domain_names, replace(v.domain_name, "*.", ""))]
}

resource "aws_route53_record" "main" {
  count           = length(local.domain_names)
  zone_id         = data.aws_route53_zone.main.id
  name            = element(local.validation_domains, count.index)["resource_record_name"]
  type            = element(local.validation_domains, count.index)["resource_record_type"]
  ttl             = var.ttl
  records         = [element(local.validation_domains, count.index)["resource_record_value"]]
  allow_overwrite = var.overwrite_dns
  depends_on      = [aws_acm_certificate.main]
}

resource "aws_acm_certificate_validation" "main" {
  certificate_arn         = aws_acm_certificate.main.arn
  validation_record_fqdns = aws_route53_record.main[*].fqdn
}
