variable "domain_name" {}

variable "dns_zone" {}

variable "subject_alternative_names" {
  type    = list(string)
  default = []
}

variable "overwrite_dns" {
  default = false
}

variable "ttl" {
  default = 300
}

variable "tags" {
  type    = map(any)
  default = {}
}
