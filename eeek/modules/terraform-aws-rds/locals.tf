locals {
  name        = var.name
  environment = var.environment == null ? terraform.workspace : var.environment
  tags = merge({
    "Name"        = local.name
    "Environment" = local.environment
  }, var.tags)
}
