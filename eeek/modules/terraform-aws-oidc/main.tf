locals {
  name          = var.name
  tags          = var.tags
  environment   = var.environment == null ? terraform.workspace : var.environment
  oidc_provider = replace(var.oidc_url, "https://", "")
  account_id    = data.aws_caller_identity.current.account_id
}

resource "aws_iam_openid_connect_provider" "main" {
  client_id_list  = [var.oidc_url]
  thumbprint_list = [var.certificate_thumbprint == null ? data.tls_certificate.main.certificates[0].sha1_fingerprint : var.certificate_thumbprint]
  url             = var.oidc_url

  tags = merge({
    Name        = local.name
    Environment = local.environment
  }, var.tags)
}

resource "aws_iam_role" "main" {
  name_prefix = "${local.name}-oidc-${local.environment}-"
  description = "${local.name}-oidc"
  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRoleWithWebIdentity"
      Effect = "Allow"
      Principal = {
        Federated = "arn:aws:iam::${local.account_id}:oidc-provider/${local.oidc_provider}"
      }
      Condition = {
        StringLike = {
          "${local.oidc_provider}:sub" = var.allowed_subjects
        }
      }
    }]
    Version = "2012-10-17"
  })

  tags = merge({
    Name        = local.name
    Environment = local.environment
  }, var.tags)
}
