variable "name" {
  type = string
}

variable "environment" {
  type    = string
  default = null
}

variable "tags" {
  default = {}
}

variable "oidc_url" {
  type = string
}

variable "allowed_subjects" {
  type = list(string)
}

variable "certificate_thumbprint" {
  type    = string
  default = null
}
