data "aws_availability_zones" "available" {}
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

data "tls_certificate" "main" {
  url = var.oidc_url
}
