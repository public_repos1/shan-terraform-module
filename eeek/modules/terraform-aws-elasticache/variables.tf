variable "name" {
  description = "Name for all resources"
  type        = string
}

variable "environment" {
  description = "Environment Name, defaults to workspace name"
  type        = string
  default     = null
}

variable "vpc_id" {
  description = "VPC to deploy ElastiCache"
  type        = string
}

variable "subnet_ids" {
  description = "Subnets to deploy ElastiCache"
  type        = list(string)
}

variable "security_group_ids" {
  type    = list(string)
  default = []
}

variable "ingress_security_group_rules" {
  type = map(any)
}

variable "tags" {
  default = {}
}

variable "password_overide_special" {
  type    = string
  default = "#"
}

variable "allow_immediate_secret_deletion" {
  type    = bool
  default = false
}
