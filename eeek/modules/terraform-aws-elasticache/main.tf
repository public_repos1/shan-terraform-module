locals {
  environment = var.environment == null ? terraform.workspace : var.environment
  tags = merge({
    Name        = var.name
    Environment = local.environment
  }, var.tags)
  port = 6379
}

resource "aws_elasticache_subnet_group" "main" {
  name        = var.name
  description = var.name
  subnet_ids  = var.subnet_ids
}

resource "aws_elasticache_parameter_group" "main" {
  name        = var.name
  description = var.name
  family      = "redis6.x"
}

resource "random_password" "main" {
  length           = 16
  override_special = var.password_overide_special
  special          = false
  lifecycle {
    ignore_changes = all
  }
}

resource "aws_elasticache_replication_group" "main" {
  replication_group_id       = var.name
  description                = var.name
  engine                     = "redis"
  node_type                  = "cache.r6g.large"
  parameter_group_name       = aws_elasticache_parameter_group.main.name
  engine_version             = "6.x"
  port                       = local.port
  transit_encryption_enabled = true
  at_rest_encryption_enabled = true
  auth_token                 = random_password.main.result
  subnet_group_name          = aws_elasticache_subnet_group.main.name
  security_group_ids         = concat(tolist([aws_security_group.main.id]), var.security_group_ids)
  num_cache_clusters         = 1
  tags                       = local.tags
  lifecycle {
    ignore_changes = [engine_version]
  }
}

resource "aws_secretsmanager_secret" "redis_password" {
  name                    = "${local.environment}/redis/password"
  recovery_window_in_days = var.allow_immediate_secret_deletion ? 0 : 30
  description             = "Redis password"
  tags                    = local.tags
}

resource "aws_secretsmanager_secret_version" "redis_password" {
  secret_id     = aws_secretsmanager_secret.redis_password.id
  secret_string = random_password.main.result
}

resource "aws_secretsmanager_secret" "redis_host" {
  name                    = "${local.environment}/redis/host"
  recovery_window_in_days = var.allow_immediate_secret_deletion ? 0 : 30
  description             = "Redis host"
  tags                    = local.tags
}

resource "aws_secretsmanager_secret_version" "redis_host" {
  secret_id     = aws_secretsmanager_secret.redis_host.id
  secret_string = "${aws_elasticache_replication_group.main.primary_endpoint_address}:${local.port}"
}
