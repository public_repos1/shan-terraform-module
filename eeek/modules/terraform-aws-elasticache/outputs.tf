output "subnet_group" {
  value = aws_elasticache_subnet_group.main
}

output "primary_endpoint" {
  value = aws_elasticache_replication_group.main.primary_endpoint_address
}
