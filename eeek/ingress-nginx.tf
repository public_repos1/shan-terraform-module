locals {
  ingress_nginx_namespace = "kube-system"
  ingress_nginx_name      = "ingress-nginx"
}

resource "helm_release" "ingress_nginx" {
  namespace  = local.ingress_nginx_namespace
  name       = local.ingress_nginx_name
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.2.0"

  set {
    name  = "service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-cert"
    value = module.acm.arn
  }

  values = [
    file("./files/nginx-values.yaml")
  ]

  depends_on = [
    module.eks
  ]
}

# This is not used at this moment as we use NLB
resource "aws_security_group" "ingress_nginx" {
  name_prefix = local.ingress_nginx_name
  description = "allow http and https for ${local.ingress_nginx_name} alb"
  vpc_id      = module.vpc.id

  dynamic "ingress" {
    for_each = var.allowed_cidrs
    content {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ingress.value
      description = ingress.key
    }
  }

  dynamic "ingress" {
    for_each = var.allowed_cidrs
    content {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ingress.value
      description = ingress.key
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "kubernetes_service" "ingress_nginx" {
  metadata {
    namespace = local.ingress_nginx_namespace
    name      = "${local.ingress_nginx_name}-controller"
  }
  depends_on = [
    helm_release.ingress_nginx,
    aws_security_group.ingress_nginx
  ]
}
