
# Equilibrium Terraform

Terraform should be run by Gitlab CI/CD only unless in special cases.

####  Usage (GitLab CI/CD)
To make any change to terraform create a branch from `master` as `feature/<JIRA_TICKET_ID>` or `feature/<NAME>` if the change is not associated with a jira ticket.
The `terraform plan` will run on every push on the branch as well as when creating a PR. When the plan looks good across all environments, merging to `master` will run `terraform apply` which is manual for now.

When merged/pushed to `master` branch, `terraform plan` will run automatically on all environments and wait for `terraform apply` to be triggered manually.

#### Best Practices

 - Use modules to introduce a new set of changes that should go together
 - Keep the changes backward compatible and optional. Example
	  ```count = var.should_this_be_deployed ? 1 : 0```
- Avoid hard coding and pass it via variables.

#### Local Usage (Special Circumstances Only)

To run the terraform from a fresh clone first initialize the terraform, then
switch into target environment's workspace and then either run a `plan` to see
what changes will be executed or run an `apply` to see the diff and then
approve it for execution when prompted.

```bash
terraform init
terraform workspace select {dev|stage|qa}
terraform {plan|apply} -var-file tfvars/{dev|stage|qa}.tfvars
```

The initial `terraform init` only needs to be run once per clone.

If you pushed changes to a submodule in another repo then run the following to
pull them in:

```bash
terraform get -update
```

#### Variables

Refer to `tfvars/{dev|stage|qa}.tfvars` files for existing environment specific variables.

|Variable        |Type               |Default                     |Description                                                                                            |Example                                         |
|----------------|-------------------|----------------------------|-------------------------------------------------------------------------------------------------------|------------------------------------------------|
|`aws_profile`   |`string`           |`equilibrium/stage`         |AWS Profile to use (awscli: `aws configure`                                                            |                                                |
|`aws_region`    |`string`           |`us-east-1`                 |AWS Region to deploy                                                                                   |`us-east-1`                                     |
|`name`          |`string`           |`eeek`                      |Name of the resources. Mostly `name-environment` pattern is used everywhere, see `environment` below   |`eeek`                                          |
|`eks_version`   |`string`           |`1.20`                      |EKS cluster version. Only incrementing supported.                                                      |`1.21`                                          |
|`environment`   |`string`           |`null`                      |Environment name. If left `null`, current terraform workspace name is being used                       |`uat`                                           |
|`cidr`          |`string`           |`10.20.0.0/16`              |VPC CIDR (make sure not to overlap with existing environments)                                         |`10.100.0.0/16`                                 |
|`dns_zone`      |`string`           |                            |DNS Zone in Route53 specific to the environment (will be created)                                      |`qa.eeek.io`                                    |
|`root_dns_zone` |`string`           |`eeek.io`                   |Root DNS zone, that needs to exist.                                                                    |`eeek.io`                                       |
|`tags`          |`map(string)`      |`{}`                        |Common tags for all resources                                                                          |`{Project = "Equilibrium", Environment = "UAT"}`|
|`allowed_cidrs` |`map(list(string))`|`{"world" = ["0.0.0.0/0"]}` |This is used on Nginx Security group only to open port `80` (HTTP) and port `443` (HHTPS) to the world`|`{"security_group_name" = ["10.0.0.0/16", "10.1.0.0/16"]}`|


#### Refreshing the QA DB
1. install terraform as indicated above, psql, and aws cli.
2. make sure you have an equilibrium/stage profile in your aws credentials file.  Install the aws cli
3. run `./scripts/qa_db_refresh.sh`.  This will create a snapshot of the stage DB, delete the existing eeek-qa rds instance and instantiate a new eeek-qa instance from said snapshot. It will also change the eq_stage DB name in the new instance to eq_qa
4. In the eeek-qa kubernetes cluster, delete the hasura pod and the all the temporal pods (in the temporal namespace).
5. You should now be able to trigger [RWO](https://rwo-trigger.qa.eeek.io/) and create trades in the QA DB


### Setup IRSA for you application. Per Application AWS API access.

In EKS we can attach IAM Roles to containers, so that they have the least privilege. The IRSA setup is defined as a template in `irsa_app.tf` file. To create a role for a new app, say `newapp`, do the following
1. Clone the `irsa_app.tf` to `irsa_newapp.tf` and rename all occurrences on `app` in the file to `newapp`
```
cp irsa_app.tf irsa_newapp.tf
sed -i'' -e 's/app/newapp/g' irsa_newapp.tf
```
2. Properly set `create_namespace` and `create_service_account` flags on line numbers `21` and `22` based on the use case.
3. Modify the IAM Policy object as desired.
4. Add a variable `irsa_newapp = true` in all the `tfvars` of environments that needs the role. Example, set it in `tfvars/dev.tfvars` to deploy the same in dev environment.
For more details: https://docs.aws.amazon.com/eks/latest/userguide/iam-roles-for-service-accounts.html

### Using the service account in the application
1. Configure the Deployment / Statefulset to use the service account created above.
```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: sample-app
  namespace: sample-ns
spec:
  replicas: 1
  selector:
    matchLabels:
      app: sample-app
  template:
    metadata:
      labels:
        app: sample-app
    spec:
      containers:
        - name: server
      ....
      serviceAccountName: <SERVICE_ACCOUNT_NAME>
      serviceAccount: <SERVICE_ACCOUNT_NAME>
      ....
```
2. Configure AWS SDK/CLI to use its default credential chain. Simply do not specify any credentials, configure only region_name in SDK/CLI.
```
ec2 = boto3.client('ec2', region_name='us-east-1')
```
3. Remove any configured static credentials file and env variables on the deployments/statefulsets.
For more details: https://equilibriumenergy.atlassian.net/wiki/spaces/ENG/pages/855441422/Security+Best+Practices