# This is  a template for adding IRSA (per application AWS API access). Please DONOT change this file, duplicate this file and rename it and use accordingly.
# Replace all occurance of the keyword `app` with the desired application name after duplciating this file, example `apollo`.

variable "irsa_app" {
  type    = bool
  default = false
}

locals {
  app_name      = "app"
  app_namespace = "app-namespace"
}

module "irsa_app" {
  count                  = var.irsa_app ? 1 : 0
  source                 = "./modules/terraform-aws-eks-irsa"
  cluster_name           = module.eks.eks.id
  name                   = local.app_name
  namespace              = local.app_namespace
  service_account        = local.app_name
  create_namespace       = false # `true` will create the the namespace (managed via Terraform). `false` will expect the namespace to exist already (managed outside of Terraform).
  create_service_account = false  # `true` will create the service account with proper annotation. `false` will set proper annotation to the existing service account(service account should exists)
}

resource "aws_iam_role_policy" "app" {
  count       = var.irsa_app ? 1 : 0
  name_prefix = "eks-irsa-${local.name}-app-"
  role        = module.irsa_app[0].iam_role_name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ssm:GetParameter"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

output "irsa_app" {
  value = module.irsa_app
}
