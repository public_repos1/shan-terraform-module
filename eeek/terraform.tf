terraform {
  backend "s3" {
    bucket               = "equilibrium-terraform"
    workspace_key_prefix = "equilibrium"
    key                  = "terraform.tfstate"
    encrypt              = true
    region               = "us-east-1"
    acl                  = "bucket-owner-full-control"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.47.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.16.1"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.8.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.4.3"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.4"
    }
  }
}
