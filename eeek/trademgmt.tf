resource "aws_secretsmanager_secret" "trademgmt_auth_token" {
  name                    = "${local.environment}/trademgmt/auth_token"
  recovery_window_in_days = var.allow_immediate_secret_deletion ? 0 : 30
}

resource "random_password" "trademgmt_auth_token" {
  length  = 16
  special = false
}

resource "aws_secretsmanager_secret_version" "trademgmt_auth_token" {
  secret_id     = aws_secretsmanager_secret.trademgmt_auth_token.id
  secret_string = random_password.trademgmt_auth_token.result
}
