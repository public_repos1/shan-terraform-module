resource "aws_iam_policy" "workers_sagemaker_stage" {
  count       = terraform.workspace == "stage" ? 0 : 1
  name        = "AssumeSagemakerCrossAccountRole"
  path        = "/"
  description = "IAM Policy for Sagemaker's Feature Store Access in stage"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action   = "sts:AssumeRole"
      Effect   = "Allow"
      Resource = "arn:aws:iam::651458935300:role/SagemakerCrossAccountRole"
      Sid      = "AllowToAssumeSagemakerCrossAccountRoleonStage"
    }]
  })
}

resource "aws_iam_role_policy_attachment" "workers_sagemaker_stage" {
  count      = terraform.workspace == "stage" ? 0 : 1
  policy_arn = aws_iam_policy.workers_sagemaker_stage[0].arn
  role       = module.eks.worker_iam_role_name
}
