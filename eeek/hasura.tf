resource "aws_secretsmanager_secret" "hasura_secret" {
  name                    = "${local.environment}/RDS/hasura_admin_secret"
  recovery_window_in_days = var.allow_immediate_secret_deletion ? 0 : 30
}

resource "random_password" "hasura_secret" {
  length  = 16
  special = false
}

resource "aws_secretsmanager_secret_version" "hasura_secret" {
  secret_id     = aws_secretsmanager_secret.hasura_secret.id
  secret_string = random_password.hasura_secret.result
}

resource "aws_secretsmanager_secret" "hasura_url" {
  name                    = "${local.environment}/RDS/hasura_url"
  recovery_window_in_days = var.allow_immediate_secret_deletion ? 0 : 30
}

resource "aws_secretsmanager_secret_version" "hasura_url" {
  secret_id     = aws_secretsmanager_secret.hasura_url.id
  secret_string = "http://hasura.hasura-28590029-${local.environment}.svc.cluster.local:80/v1/graphql"
}
