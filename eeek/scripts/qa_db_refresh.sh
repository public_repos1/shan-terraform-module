set -e
NOW=`date "+%Y-%m-%d-%H%M%S"`
SNAPSHOT_NAME=qa-eeek-stage-${NOW}

# create snapshot from stage DB and wait for it to complete
aws --profile equilibrium/stage rds create-db-snapshot \
    --db-instance-identifier eeek-stage \
    --db-snapshot-identifier $SNAPSHOT_NAME
aws rds wait db-snapshot-completed --db-snapshot-identifier $SNAPSHOT_NAME --profile equilibrium/stage

#reinstantiate qa DB from the snapshot above and wait for it to complete
terraform apply -var-file tfvars/qa.tfvars -var snapshot_identifier=${SNAPSHOT_NAME} -var deletion_protection=false
aws rds wait db-instance-available --db-instance-identifier eeek-qa --profile equilibrium/stage

qa_db_user_password=$(aws secretsmanager get-secret-value --secret-id qa/RDS/postgres --profile equilibrium/stage | python -c "import sys, json; print(json.loads(json.load(sys.stdin)['SecretString'])['password'])")
export PGPASSWORD=$qa_db_user_password
psql -h db.qa.eeek.io -U postgres -c "ALTER DATABASE eq_stage RENAME TO eq_qa;"

# restart the hasura and temporal pods
kubectl --context=equilibrium/eeek-qa --namespace=hasura-28590029-qa delete po --all
kubectl --context=equilibrium/eeek-qa --namespace=temporal delete po --all