data "aws_availability_zones" "available" {}
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

data "aws_caller_identity" "devops" {
  provider = aws.devops
}


data "terraform_remote_state" "devops" {
  backend   = "s3"
  workspace = "default"
  config = {
    bucket  = "equilibrium-terraform"
    key     = "devops/terraform.tfstate"
    region  = "us-east-1"
    acl     = "bucket-owner-full-control"
    encrypt = true
  }
}

data "aws_eks_cluster" "main" {
  name = module.eks.eks.id
}

data "aws_eks_cluster_auth" "main" {
  name = module.eks.eks.id
}
