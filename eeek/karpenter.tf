locals {
  karpenter_name      = "karpenter"
  karpenter_namespace = "karpenter"
}

resource "kubernetes_namespace" "karpenter" {
  metadata {
    name = "karpenter"
  }
}

data "aws_iam_policy" "karpenter" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "karpenter" {
  role       = module.eks.worker_iam_role_name
  policy_arn = data.aws_iam_policy.karpenter.arn
}

module "irsa_karpenter" {
  source                 = "./modules/terraform-aws-eks-irsa"
  cluster_name           = module.eks.eks.id
  name                   = local.karpenter_name
  create_service_account = true
  service_account        = local.karpenter_name
  namespace              = local.karpenter_namespace
  depends_on             = [kubernetes_namespace.karpenter]
}

resource "aws_iam_role_policy" "karpenter" {
  name = "AmazonEKSKarpenterPolicy"
  role = module.irsa_karpenter.iam_role_name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:CreateLaunchTemplate",
          "ec2:CreateFleet",
          "ec2:RunInstances",
          "ec2:CreateTags",
          "iam:PassRole",
          "ec2:TerminateInstances",
          "ec2:DescribeImages",
          "ec2:DescribeSpotPriceHistory",
          "ec2:DescribeLaunchTemplates",
          "ec2:DeleteLaunchTemplate",
          "ec2:DescribeInstances",
          "ec2:DescribeSecurityGroups",
          "ec2:DescribeSubnets",
          "ec2:DescribeInstanceTypes",
          "ec2:DescribeInstanceTypeOfferings",
          "ec2:DescribeAvailabilityZones",
          "ssm:GetParameter",
          "pricing:GetProducts"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "sqs:DeleteMessage",
          "sqs:GetQueueUrl",
          "sqs:GetQueueAttributes",
          "sqs:ReceiveMessage"
        ]
        Effect   = "Allow"
        Resource = aws_sqs_queue.karpenter.arn
      }
    ]
  })
}

resource "aws_sqs_queue" "karpenter" {
  name                      = "karpenter-${module.eks.eks.id}"
  message_retention_seconds = 300
  tags = {
    Environment = local.environment
  }
}

resource "aws_sqs_queue_policy" "karpenter" {
  queue_url = aws_sqs_queue.karpenter.id
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "EC2InterruptionPolicy"
    Statement = [
      {
        Principal = {
          Service = [
            "events.amazonaws.com",
            "sqs.amazonaws.com"
          ]
        }
        Action = [
          "sqs:SendMessage"
        ]
        Effect   = "Allow"
        Resource = aws_sqs_queue.karpenter.arn
      }
    ]
  })
}

locals {
  karpenter_events_rules = {
    scheduled-change = {
      description = "AWS Health Event"
      event_pattern = {
        source      = ["aws.health"]
        detail-type = ["AWS Health Event"]
      }
    }
    spot-interruption = {
      description = "EC2 Spot Instance Interruption Warning"
      event_pattern = {
        source      = ["aws.ec2"]
        detail-type = ["EC2 Spot Instance Interruption Warning"]
      }
    }
    rebalance-rule = {
      description = "EC2 Instance Rebalance Recommendation"
      event_pattern = {
        source      = ["aws.ec2"]
        detail-type = ["EC2 Instance Rebalance Recommendation"]
      }
    }
    instance-state-change = {
      description = "EC2 Instance State-change Notification"
      event_pattern = {
        source      = ["aws.ec2"]
        detail-type = ["EC2 Instance State-change Notification"]
      }
    }
  }
}

resource "aws_cloudwatch_event_rule" "karpenter" {
  for_each      = local.karpenter_events_rules
  name_prefix   = "karpenter-ec2-${each.key}"
  description   = each.value["description"]
  event_pattern = jsonencode(each.value["event_pattern"])
}

resource "aws_cloudwatch_event_target" "karpenter" {
  for_each  = aws_cloudwatch_event_rule.karpenter
  rule      = each.value["name"]
  target_id = "KarpenterInterruptionQueueTarget"
  arn       = aws_sqs_queue.karpenter.arn
}

resource "helm_release" "karpenter" {
  name             = local.karpenter_name
  namespace        = kubernetes_namespace.karpenter.metadata[0].name
  create_namespace = false

  chart   = "oci://public.ecr.aws/karpenter/karpenter"
  version = "v0.20.0"

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value = "karpenter"
  }

  set {
    name  = "settings.aws.clusterName"
    value = module.eks.eks.id
  }

  set {
    name  = "settings.aws.clusterEndpoint"
    value = module.eks.eks.endpoint
  }

  set {
    name  = "settings.aws.interruptionQueueName"
    value = aws_sqs_queue.karpenter.name
  }

  set {
    name  = "nodeSelector.name"
    value = "system"
  }

  set {
    name  = "settings.aws.defaultInstanceProfile"
    value = module.eks.worker_instance_profile
  }

  depends_on = [
    kubernetes_namespace.karpenter,
    module.irsa_karpenter,
    aws_iam_role_policy_attachment.karpenter,
    aws_iam_role_policy.karpenter,
    module.eks
  ]
}

/*
resource "kubernetes_manifest" "karpenter_provisioner_default" {
  count           = var.initial_deploy ? 0 : 1
  computed_fields = ["spec.requirements"]
  manifest = {
    apiVersion = "karpenter.sh/v1alpha5"
    kind       = "Provisioner"
    metadata = {
      name = "default"
    }
    spec = {
      provider = {
        apiVersion      = "extensions.karpenter.sh/v1alpha1"
        kind            = "AWS"
        instanceProfile = module.eks.worker_instance_profile
        blockDeviceMappings = [{
          deviceName = "/dev/xvda"
          ebs = {
            deleteOnTermination = true
            encrypted           = true
            volumeSize          = "100Gi"
            volumeType          = "gp3"
          }
        }]
        subnetSelector = {
          "karpenter.sh/discovery" = module.eks.eks.id
        }
        securityGroupSelector = {
          "kubernetes.io/cluster/${module.eks.eks.id}" = "owned"
        }
      }
      limits = {
        resources = {
          cpu    = "100"
          memory = "200Gi"
        }
      }
      requirements = [{
        key      = "kubernetes.io/arch"
        operator = "In"
        values   = ["amd64"]
        }, {
        key      = "karpenter.sh/capacity-type"
        operator = "In"
        values = [
          "spot",
          "on-demand"
        ]
      }]
      labels = {
        name = "default"
      }
      ttlSecondsAfterEmpty   = 30
      ttlSecondsUntilExpired = 2592000
    }
  }

  field_manager {
    force_conflicts = true
  }
}

resource "kubernetes_manifest" "karpenter_provisioner_gpu" {
  count = var.initial_deploy ? 0 : 1
  manifest = {
    apiVersion = "karpenter.sh/v1alpha5"
    kind       = "Provisioner"
    metadata = {
      name = "gpu"
    }
    spec = {
      providerRef = {
        name = "gpu"
      }
      # The limits here are for safety on cost, that we don't end up too running too many GPU instances
      limits = {
        resources = {
          "cpu" : "400"
          "memory" : "1600Gi"
          "nvidia.com/gpu" : "24"
        }
      }
      requirements = [{
        key      = "karpenter.sh/capacity-type"
        operator = "In"
        values = [
          "on-demand"
        ]
        }, {
        key      = "kubernetes.io/arch"
        operator = "In"
        values   = ["amd64"]
        }, {
        key      = "node.kubernetes.io/instance-type"
        operator = "In"
        values = [
          "g4dn.xlarge",
          "g4dn.2xlarge",
          "g4dn.4xlarge",
          "g4dn.8xlarge",
          "g5.xlarge",
          "g5.2xlarge",
          "g5.4xlarge",
          "g5.8xlarge"
        ]
      }]
      labels = {
        name       = "gpu"
        node_group = "gpu"
      }
      taints = [{
        effect = "NoSchedule"
        key    = "instance"
        value  = "gpu"
      }]
      ttlSecondsAfterEmpty   = 120
      ttlSecondsUntilExpired = 2592000
    }
  }

  field_manager {
    force_conflicts = true
  }
}

resource "kubernetes_manifest" "karpenter_provider_gpu" {
  count = var.initial_deploy ? 0 : 1
  manifest = {
    apiVersion = "karpenter.k8s.aws/v1alpha1"
    kind       = "AWSNodeTemplate"
    metadata = {
      name = "gpu"
    }
    spec = {
      blockDeviceMappings = [{
        deviceName = "/dev/xvda"
        ebs = {
          deleteOnTermination = true
          encrypted           = true
          volumeSize          = "100Gi"
          volumeType          = "gp3"
        }
      }]
      subnetSelector = {
        "karpenter.sh/discovery" = module.eks.eks.id
      }
      securityGroupSelector = {
        "kubernetes.io/cluster/${module.eks.eks.id}" = "owned"
      }
      instanceProfile = module.eks.worker_instance_profile
      tags = {
        Name = "eks-${module.eks.eks.id}-gpu"
      }
    }
  }
}
*/
