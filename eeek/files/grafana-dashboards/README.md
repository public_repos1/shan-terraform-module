# Grafana Dashboards
The dashboards in this folder are included in new installations/upgrades of Grafana via [Helm](../../monitoring_logging.tf#62). Some of these dashboards have been sourced from the community and customized while others were developed in-house.

## Modifying Dashboards
To modify dashboards in this directory:
1. Edit the dashboard at [grafana.dev.eeek.io](https://grafana.dev.eeek.io) and export the JSON file.
1. Push the exported JSON file back to this directory in GitLab (create a new branch and perform a merge request).
1. Perform a `helm upgrade` via Terraform using the pipeline.