variable "irsa_external_dns" {
  type    = bool
  default = true
}

locals {
  external_dns_name      = "external-dns"
  external_dns_namespace = "kube-system"
}

module "irsa_external_dns" {
  count                  = var.irsa_external_dns ? 1 : 0
  source                 = "./modules/terraform-aws-eks-irsa"
  cluster_name           = module.eks.eks.id
  name                   = local.external_dns_name
  namespace              = local.external_dns_namespace
  service_account        = local.external_dns_name
  create_namespace       = false # `true` will create the the namespace (managed via Terraform). `false` will expect the namespace to exist already (managed outside of Terraform).
  create_service_account = true  # `true` will create the service account with proper annotation. `false` will set proper annotation to the existing service account(service account should exists)
}

resource "aws_iam_role_policy" "external_dns" {
  count       = var.irsa_external_dns ? 1 : 0
  name_prefix = "eks-irsa-${local.name}-external-dns-"
  role        = module.irsa_external_dns[0].iam_role_name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Effect" : "Allow",
        "Action" : [
          "route53:ChangeResourceRecordSets"
        ],
        "Resource" : [
          "arn:aws:route53:::hostedzone/*"
        ]
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "route53:ListHostedZones",
          "route53:ListResourceRecordSets"
        ],
        "Resource" : [
          "*"
        ]
      }
    ]
  })
}

resource "helm_release" "external_dns" {
  count      = var.irsa_external_dns ? 1 : 0
  name       = local.external_dns_name
  namespace  = local.external_dns_namespace
  repository = "https://kubernetes-sigs.github.io/external-dns/"
  chart      = "external-dns"

  values = [
    templatefile("./files/external-dns-values.yaml", {
      dns_zone             = var.dns_zone
      service_account_name = local.external_dns_name
    })
  ]

  depends_on = [
    module.irsa_external_dns,
    module.eks
  ]
}

output "irsa_external_dns" {
  value = module.irsa_external_dns
}
