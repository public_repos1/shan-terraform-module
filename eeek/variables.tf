variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "aws region"
}

variable "aws_role_arn" {
  default     = null
  description = "aws role arn"
}

variable "aws_external_id" {
  default     = null
  description = "aws external id"
}

variable "name" {
  type        = string
  default     = "eeek"
  description = "name of the vpc"
}

variable "deploy_airflow" {
  type    = bool
  default = true
}

variable "airflow_version" {
  type    = string
  default = "2.0.2"
}

variable "eks_version" {
  type        = string
  default     = "1.21"
  description = "eks version"
}

variable "environment" {
  type        = string
  default     = null
  description = "environment name, if set to terraform workspace name if not set"
}

variable "snapshot_identifier" {
  type    = string
  default = null
}

variable "cidr" {
  type        = string
  default     = "10.20.0.0/16"
  description = "vpc cidr"
}

variable "dns_zone" {
  type        = string
  description = "dns zone for the environment"
}

variable "subject_alternative_names" {
  type        = list(string)
  description = "subject alternative names"
  default     = []
}

variable "root_dns_zone" {
  type    = string
  default = "eeek.io"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Tags"
}

variable "allowed_cidrs" {
  default = {
    "world" = ["0.0.0.0/0"]
  }
}

variable "postgres_db_name" {
  default = null
}

variable "rds_deletion_protection" {
  type    = bool
  default = true
}

variable "rds_max_allocated_storage" {
  type    = number
  default = 65536
}

variable "rds_storage_allocated" {
  type    = number
  default = 100
}

variable "random_password_overide_special" {
  default = "#"
}

variable "deploy_sagmaker" {
  type    = bool
  default = true
}

variable "sagemaker_domain" {
  type    = string
  default = "default"
}

variable "sagemaker_s3_output_path" {
  default = null
}

variable "rds_performance_insights_enabled" {
  type    = bool
  default = true
}

variable "deploy_backup" {
  type    = bool
  default = true
}

variable "deploy_gitlab_oidc" {
  type    = bool
  default = false
}

variable "default_storageclass" {
  type    = string
  default = "gp2"
}

variable "airflow_min_workers" {
  type    = number
  default = 2
}

variable "airflow_max_workers" {
  type    = number
  default = 2
}

variable "deploy_gitlab_agent" {
  type    = bool
  default = false
}

variable "gitlab_agent_token" {
  type    = string
  default = ""
}

variable "initial_deploy" {
  type    = bool
  default = false
}

variable "database_engine_version" {
  type    = string
  default = "12.5"
}

variable "allow_immediate_secret_deletion" {
  type    = bool
  default = false
}

variable "source_aws_role_arn" {
  type    = string
  default = null
}

variable "clone_db" {
  type    = bool
  default = false
}

variable "source_db_instance_identifier" {
  type    = string
  default = null
}

variable "rds_enable_cmk" {
  type    = bool
  default = false
}

variable "iam_database_authentication_enabled" {
  type    = bool
  default = false
}

variable "additional_vpc_tags" {
  default = {}
}

variable "google_oauth_client_id" {
  description = "Google OAuth Client ID, for establishing SSO with Grafana."
  type        = string
}

variable "google_oauth_client_secret" {
  description = "Google OAuth Client Secret, for establishing SSO with Grafana."
  type        = string
}

variable "prometheus_remote_write_url" {
  type = string
}

variable "deploy_loki" {
  type        = bool
  default     = true
  description = "Conditionally deploy Loki"
}

variable "deploy_prometheus_adapter" {
  type        = bool
  default     = false
  description = "Conditionally deploy Loki"
}
