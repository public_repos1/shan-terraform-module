module "database_kms" {
  count  = var.rds_enable_cmk ? 1 : 0
  source = "./modules/terraform-aws-kms"
  name   = "/rds/${local.name}"
  tags = {
    Environment = local.environment
  }
}

resource "random_password" "database" {
  length  = 16
  special = false
}

# Currently the AWS Provider doesnot support sharing database snapshots, until this featture is implemented, we are restorting to AWS CLI with local-exec to achieve the same.
# https://github.com/hashicorp/terraform-provider-aws/issues/3860
resource "aws_db_snapshot" "source_db" {
  provider               = aws.source
  count                  = var.clone_db ? 1 : 0
  db_instance_identifier = var.source_db_instance_identifier
  db_snapshot_identifier = "${var.source_db_instance_identifier}-${formatdate("YYYYMMDDhhmmss", timestamp())}"
  provisioner "local-exec" {
    command = <<-EOF
      ASSUME_ROLE_OUT=$(aws sts assume-role --role-arn "${var.source_aws_role_arn}" --role-session-name terraform-local-exec)

      export AWS_ACCESS_KEY_ID=$(echo $ASSUME_ROLE_OUT | jq -r '.Credentials''.AccessKeyId')
      export AWS_SECRET_ACCESS_KEY=$(echo $ASSUME_ROLE_OUT | jq -r '.Credentials''.SecretAccessKey')
      export AWS_SESSION_TOKEN=$(echo $ASSUME_ROLE_OUT | jq -r '.Credentials''.SessionToken')

      aws rds modify-db-snapshot-attribute \
        --db-snapshot-identifier ${self.db_snapshot_identifier} \
        --attribute-name "restore" \
        --values-to-add '["${data.aws_caller_identity.current.account_id}"]'
    EOF
  }

  timeouts {
    read = "60m"
  }
}

resource "aws_db_snapshot_copy" "source_db_copy" {
  count                         = var.clone_db ? 1 : 0
  source_db_snapshot_identifier = aws_db_snapshot.source_db[0].db_snapshot_arn
  target_db_snapshot_identifier = aws_db_snapshot.source_db[0].db_snapshot_identifier
  kms_key_id                    = var.rds_enable_cmk ? module.database_kms[0].arn : null

  timeouts {
    create = "60m"
  }
}

locals {
  rds_snapshot_identifier = (var.clone_db && var.snapshot_identifier == null) ? aws_db_snapshot_copy.source_db_copy[0].id : var.snapshot_identifier
}

module "database" {
  source                          = "./modules/terraform-aws-rds"
  name                            = local.name
  vpc_id                          = module.vpc.id
  subnet_ids                      = module.vpc.database_subnets
  allow_immediate_secret_deletion = var.allow_immediate_secret_deletion

  engine = {
    name    = "postgres"
    version = var.database_engine_version
    family  = "postgres12"
  }

  instance_class = "db.t4g.xlarge"
  storage = {
    allocated  = local.rds_snapshot_identifier == null ? var.rds_storage_allocated : null
    type       = local.rds_snapshot_identifier == null ? "gp2" : null
    encrypted  = true
    kms_key_id = var.rds_enable_cmk ? module.database_kms[0].arn : null
  }
  database_username                   = "postgres"
  database_name                       = var.postgres_db_name == null ? "eq_${local.environment}" : var.postgres_db_name
  database_password                   = random_password.database.result
  port                                = 5432
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  security_group_ids                  = module.airflow[*].security_group_id

  multi_az                              = false
  publicly_accessible                   = false
  allow_major_version_upgrade           = false
  auto_minor_version_upgrade            = false
  apply_immediately                     = true
  skip_final_snapshot                   = true
  copy_tags_to_snapshot                 = true
  final_snapshot_identifier             = null
  max_allocated_storage                 = var.rds_max_allocated_storage
  enhanced_monitoring                   = false
  monitoring_role_arn                   = ""
  performance_insights_enabled          = var.rds_performance_insights_enabled
  performance_insights_retention_period = 7
  backup_retention_period               = 7
  enabled_cloudwatch_logs_exports       = []
  deletion_protection                   = var.rds_deletion_protection
  snapshot_identifier                   = local.rds_snapshot_identifier
  ingress_security_group_rules = {
    "allow_vpn_instance_sg" = {
      source_security_group_id = local.devops.openvpn.sg_id
    }
    "allow_eks_cluster_sg" = {
      from_port                = "5432"
      to_port                  = "5432"
      source_security_group_id = module.eks.cluster_security_group_id
    }
    "allow_eks_self_managed_workers_sg" = {
      from_port                = "5432"
      to_port                  = "5432"
      source_security_group_id = module.eks.workers_security_group_id
    }
    "allow_eks_managed_workers_sg" = {
      from_port                = "5432"
      to_port                  = "5432"
      source_security_group_id = module.eks.eks.vpc_config[0].cluster_security_group_id
    }
    "allow_all_self_vpc" = {
      from_port   = "5432"
      to_port     = "5432"
      cidr_blocks = module.vpc.cidr
    }
  }
  parameters = [
    {
      name  = "log_statement"
      value = "ddl"
    },
    {
      name  = "log_min_duration_statement"
      value = "60000"
    },
  ]
  depends_on = [aws_vpc_peering_connection.main]
}
