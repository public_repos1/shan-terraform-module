name                            = "eeek"
environment                     = "stage"
cidr                            = "10.30.0.0/16"
dns_zone                        = "stage.eeek.io"
postgres_db_name                = "eq_stage"
random_password_overide_special = "!#$%&*-_=+<>?"
airflow_version                 = "2.2.2"
aws_role_arn                    = "arn:aws:iam::651458935300:role/OrganizationAccountAccessRole"
deploy_sagmaker                 = false

airflow_min_workers = 5
airflow_max_workers = 15