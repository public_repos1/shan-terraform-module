name                     = "eeek"
environment              = "dev"
cidr                     = "10.20.0.0/16"
dns_zone                 = "dev.eeek.io"
subject_alternative_names = ["*.sl.dev.eeek.io"]
aws_role_arn             = "arn:aws:iam::142519002547:role/OrganizationAccountAccessRole"
snapshot_identifier      = ""
airflow_version          = "2.2.2"
airflow_max_workers      = 10
sagemaker_domain         = "default-1646728616318"
sagemaker_s3_output_path = "s3://dev-sagemaker-studio-142519002547-pp9x4hnyicq/sharing"
default_storageclass     = "gp3-xfs"
airflow_min_workers      = 1

iam_database_authentication_enabled = true

irsa_pinot = true
deploy_gitlab_oidc = true

prometheus_remote_write_url = "https://aps-workspaces.us-east-1.amazonaws.com/workspaces/ws-36c31f12-d9e6-42c2-bec3-5a7e47bf202e/api/v1/remote_write"