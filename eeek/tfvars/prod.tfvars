name                            = "eeek"
environment                     = "prod"
cidr                            = "10.100.0.0/16"
dns_zone                        = "prod.eeek.io"
postgres_db_name                = "eq_prod"
random_password_overide_special = "!#$%&*-_=+<>?"
airflow_version                 = "2.2.2"
default_storageclass            = "gp3-xfs"
aws_role_arn                    = "arn:aws:iam::136736264758:role/OrganizationAccountAccessRole"
deploy_sagmaker                 = false
initial_deploy                  = false
deploy_gitlab_agent             = true
allow_immediate_secret_deletion = true

clone_db                      = false
source_aws_role_arn           = "arn:aws:iam::651458935300:role/OrganizationAccountAccessRole"
source_db_instance_identifier = "eeek-stage"
rds_snapshot_identifier       = "eeek-stage-20221114003050"
rds_deletion_protection       = false
rds_enable_cmk                = true

gitlab_agent_token = "ryGQn-MaHZsczmZu3koF7LxG5vAo6Lung25NZQFU1CBZBygErA"