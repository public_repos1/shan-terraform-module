name                             = "eeek"
environment                      = "qa"
cidr                             = "10.40.0.0/16"
dns_zone                         = "qa.eeek.io"
eks_version                      = "1.21"
random_password_overide_special  = "!#$%&*-_=+<>?"
aws_role_arn                     = "arn:aws:iam::651458935300:role/OrganizationAccountAccessRole"
rds_performance_insights_enabled = false
deploy_sagmaker                  = false
deploy_backup                    = false
airflow_min_workers              = 1
database_engine_version          = 12.8
rds_deletion_protection          = false
rds_max_allocated_storage        = null
rds_storage_allocated            = null

deploy_airflow = false