output "network" {
  value = module.vpc
}

output "devops" {
  value = local.devops.openvpn.sg_id
}

output "eks" {
  value = module.eks
}
