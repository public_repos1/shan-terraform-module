resource "aws_vpc_peering_connection" "main" {
  vpc_id        = module.vpc.id
  peer_owner_id = data.aws_caller_identity.devops.account_id
  peer_vpc_id   = local.devops.network.id
  auto_accept   = false

  tags = {
    Name = "${local.name} <> ${local.devops.name}"
  }
}

resource "aws_vpc_peering_connection_accepter" "main" {
  provider                  = aws.devops
  vpc_peering_connection_id = aws_vpc_peering_connection.main.id
  auto_accept               = true
  tags = {
    Name = "${local.name} <> ${local.devops.name}"
  }
}

resource "aws_route" "main" {
  count                     = length(module.vpc.route_tables)
  route_table_id            = module.vpc.route_tables[count.index]
  destination_cidr_block    = local.devops.network.cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.main.id
  depends_on                = [module.vpc, aws_vpc_peering_connection_accepter.main, aws_vpc_peering_connection.main]
  lifecycle {
    create_before_destroy = false
  }
}

data "aws_route_tables" "devops" {
  provider = aws.devops
  vpc_id   = local.devops.network.id
}

resource "aws_route" "devops" {
  provider                  = aws.devops
  for_each                  = toset(data.aws_route_tables.devops.ids)
  route_table_id            = each.value
  destination_cidr_block    = module.vpc.cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.main.id
  depends_on                = [module.vpc, aws_vpc_peering_connection_accepter.main, aws_vpc_peering_connection.main]
  lifecycle {
    create_before_destroy = false
  }
}

