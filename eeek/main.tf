locals {
  environment = var.environment == null ? terraform.workspace : var.environment
  name        = "${var.name}-${local.environment}"
  devops      = data.terraform_remote_state.devops.outputs
}

module "vpc" {
  source               = "./modules/terraform-aws-vpc"
  cidr                 = var.cidr
  name                 = local.name
  nat_per_az           = false
  deploy_db_subnets    = true
  subnet_outer_offsets = [4, 2, 4]
  subnet_inner_offsets = [4, 4, 4]
  allow_cidrs_default  = {}
  allow_security_groups_default = {
    OpenVPN = "${data.aws_caller_identity.devops.account_id}/${local.devops.openvpn.sg_id}"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.name}" = "shared"
    "kubernetes.io/role/elb"              = "1"
  }

  private_subnet_tags = merge({
    "kubernetes.io/cluster/${local.name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
    "karpenter.sh/discovery"              = local.name
  }, var.additional_vpc_tags)
}

resource "aws_key_pair" "main" {
  key_name_prefix = "${local.name}-"
  public_key      = file("ssh_keys/${local.environment}.pub")
}

module "eks" {
  source                        = "./modules/terraform-aws-eks"
  name                          = local.name
  eks_version                   = var.eks_version
  vpc_id                        = module.vpc.id
  subnet_ids                    = module.vpc.private_subnets
  tags                          = var.tags
  ssh_key_name                  = aws_key_pair.main.key_name
  source_security_group_ids     = tolist([module.vpc.default_sg])
  enabled_cluster_log_types     = ["api", "audit"]
  cluster_log_retention_in_days = 7
  endpoint_private_access       = true
  endpoint_public_access        = true

  node_groups = {
    "system" = {
      subnet_ids = module.vpc.private_subnets
      labels = {
        name       = "system"
        node_group = "system"
      }
      taints = [{
        key    = "CriticalAddonsOnly"
        value  = "true"
        effect = "NO_SCHEDULE"
      }]
      desired_size = local.environment == "prod" ? 4 : 3
      max_size     = local.environment == "prod" ? 4 : 3
      min_size     = local.environment == "prod" ? 4 : 3
    }
  }
  depends_on = [aws_vpc_peering_connection.main]
}

module "acm" {
  source                    = "./modules/terraform-aws-acm"
  domain_name               = "*.${var.dns_zone}"
  subject_alternative_names = var.subject_alternative_names
  dns_zone                  = var.dns_zone
  overwrite_dns             = true
  depends_on = [
    aws_route53_zone.main,
    aws_route53_record.main
  ]
}

module "dapr_redis" {
  source                          = "./modules/terraform-aws-elasticache"
  name                            = "dapr-${local.name}"
  vpc_id                          = module.vpc.id
  subnet_ids                      = module.vpc.private_subnets
  password_overide_special        = var.random_password_overide_special
  allow_immediate_secret_deletion = var.allow_immediate_secret_deletion


  ingress_security_group_rules = {
    "allow_vpn_instance_sg" = {
      from_port                = "6379"
      to_port                  = "6379"
      source_security_group_id = local.devops.openvpn.sg_id
    }
    "allow_eks_cluster_sg" = {
      from_port                = "6379"
      to_port                  = "6379"
      source_security_group_id = module.eks.cluster_security_group_id
    }
    "allow_eks_self_managed_workers_sg" = {
      from_port                = "6379"
      to_port                  = "6379"
      source_security_group_id = module.eks.workers_security_group_id
    }
    "allow_eks_managed_workers_sg" = {
      from_port                = "6379"
      to_port                  = "6379"
      source_security_group_id = module.eks.eks.vpc_config[0].cluster_security_group_id
    }
    "allow_all_self_vpc" = {
      from_port   = "6379"
      to_port     = "6379"
      cidr_blocks = module.vpc.cidr
    }
  }
  depends_on = [aws_vpc_peering_connection.main]
}

module "airflow" {
  count           = var.deploy_airflow ? 1 : 0
  source          = "./modules/terraform-aws-airflow"
  name            = local.name
  vpc_id          = module.vpc.id
  subnet_ids      = slice(module.vpc.private_subnets, 0, 2)
  airflow_version = var.airflow_version
  min_workers     = var.airflow_min_workers
  max_workers     = var.airflow_max_workers
  ingress_security_group_rules = {
    "allow_vpn_instance_sg" = {
      source_security_group_id = local.devops.openvpn.sg_id
    }
    "allow_eks_cluster_sg" = {
      source_security_group_id = module.eks.cluster_security_group_id
    }
    "allow_eks_self_managed_workers_sg" = {
      source_security_group_id = module.eks.workers_security_group_id
    }
    "allow_eks_managed_workers_sg" = {
      source_security_group_id = module.eks.eks.vpc_config[0].cluster_security_group_id
    }
    "allow_all_self_vpc" = {
      cidr_blocks = module.vpc.cidr
    }
  }
  depends_on = [aws_vpc_peering_connection.main]
}

module "sagemaker" {
  count              = var.deploy_sagmaker ? 1 : 0
  source             = "./modules/terraform-aws-sagemaker"
  name               = local.name
  sagemaker_domain   = var.sagemaker_domain
  vpc_id             = module.vpc.id
  subnet_ids         = module.vpc.private_subnets
  security_group_ids = [module.database.security_group_id]
  depends_on         = [aws_vpc_peering_connection.main]
  s3_output_path     = var.sagemaker_s3_output_path
}

module "backup" {
  count  = var.deploy_backup ? 1 : 0
  source = "./modules/terraform-aws-backup"
  name   = local.name
}

module "gitlab_oidc" {
  count                  = var.deploy_gitlab_oidc ? 1 : 0
  name                   = "gitlab"
  source                 = "./modules/terraform-aws-oidc"
  oidc_url               = "https://gitlab.com"
  certificate_thumbprint = "b3dd7606d2b5a8b4a13771dbecc9ee1cecafa38a" #Expires 2025

  allowed_subjects = [
    "project_path:equilibrium-energy/gitlab-agent:ref_type:tag:ref:*",
    "project_path:equilibrium-energy/gitlab-agent:ref_type:branch:ref:*"
  ]
}
