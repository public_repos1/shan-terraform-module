locals {
  gitlab_agent_namespace = "gitlab-agent"
  gitlab_agent_name      = "eeek"
}

resource "helm_release" "gitlab_agent" {
  count            = var.deploy_gitlab_agent ? 1 : 0
  namespace        = local.gitlab_agent_namespace
  name             = local.gitlab_agent_name
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-agent"
  create_namespace = true

  set {
    name  = "config.token"
    value = var.gitlab_agent_token
  }

  set {
    name  = "config.kasAddress"
    value = "wss://kas.gitlab.com"
  }

  depends_on = [
    module.eks
  ]
}
