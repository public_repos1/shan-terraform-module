locals {
  metrics_server_namespace = "kube-system"
  metrics_server_name      = "metrics-server"
}

module "irsa_metrics_server" {
  source                 = "./modules/terraform-aws-eks-irsa"
  cluster_name           = module.eks.eks.id
  name                   = "metrics-server"
  create_service_account = true
  service_account        = local.metrics_server_name
  namespace              = local.metrics_server_namespace
}

resource "helm_release" "metrics_server" {
  namespace  = local.metrics_server_namespace
  name       = local.metrics_server_name
  repository = "https://kubernetes-sigs.github.io/metrics-server"
  chart      = "metrics-server"

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value = local.metrics_server_name
    type  = "string"
  }

  depends_on = [
    module.eks
  ]
}
