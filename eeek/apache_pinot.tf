variable "irsa_pinot" {
  type    = bool
  default = false
}

locals {
  pinot_name      = "pinot"
  pinot_namespace = "pinot"
}

module "irsa_pinot" {
  count                  = var.irsa_pinot ? 1 : 0
  source                 = "./modules/terraform-aws-eks-irsa"
  cluster_name           = module.eks.eks.id
  name                   = local.pinot_name
  namespace              = local.pinot_namespace
  service_account        = local.pinot_name
  create_namespace       = false # `true` will create the the namespace (managed via Terraform). `false` will expect the namespace to exist already (managed outside of Terraform).
  create_service_account = false # `true` will create the service account with proper annotation. `false` will set proper annotation to the existing service account(service account should exists)
}

module "s3_pinot" {
  count            = var.irsa_pinot ? 1 : 0
  source           = "./modules/terraform-aws-s3"
  name             = "dev-eq-pinot-data"
  full_bucket_name = "dev-eq-pinot-data"
}

resource "aws_iam_role_policy" "pinot" {
  count       = var.irsa_pinot ? 1 : 0
  name_prefix = "eks-irsa-${local.name}-pinot-"
  role        = module.irsa_pinot[0].iam_role_name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:GetBucketLocation",
          "s3:ListBucket"
        ],
        "Resource" : "${module.s3_pinot[0].arn}"
      },
      {
        "Effect" : "Allow",
        "Action" : "s3:*",
        "Resource" : [
          "${module.s3_pinot[0].arn}",
          "${module.s3_pinot[0].arn}/*"
        ]
      }
    ]
  })
}

resource "helm_release" "pinot" {
  count      = var.irsa_pinot ? 1 : 0
  name       = local.pinot_name
  namespace  = local.pinot_namespace
  repository = "https://raw.githubusercontent.com/apache/pinot/master/kubernetes/helm"
  chart      = "pinot"

  values = [
    templatefile("./files/pinot-values.yaml", {
      data_bucket_name = module.s3_pinot[0].id
    })
  ]

  depends_on = [
    module.irsa_pinot,
    module.eks
  ]
}

output "irsa_pinot" {
  value = module.irsa_pinot
}
