data "aws_route53_zone" "root" {
  provider     = aws.mgmt
  name         = var.root_dns_zone
  private_zone = false
}

resource "aws_route53_zone" "main" {
  name = var.dns_zone
  tags = {
    Environment = local.environment
  }
}

resource "aws_route53_record" "main" {
  provider = aws.mgmt
  zone_id  = data.aws_route53_zone.root.zone_id
  name     = var.dns_zone
  type     = "NS"
  ttl      = "172800"
  records  = aws_route53_zone.main.name_servers
}

resource "aws_route53_record" "database" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "db.${var.dns_zone}"
  type    = "CNAME"
  ttl     = "300"
  records = [module.database.address]
}
