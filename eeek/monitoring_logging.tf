resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

resource "kubernetes_namespace" "logging" {
  metadata {
    name = "logging"
  }
}

output "kubernetes_namespace_monitoring" {
  value = kubernetes_namespace.monitoring
}

module "irsa_monitoring" {
  source                 = "./modules/terraform-aws-eks-irsa"
  cluster_name           = module.eks.eks.id
  name                   = "prometheus"
  create_service_account = false
  service_account        = "prometheus"
  namespace              = kubernetes_namespace.monitoring.metadata[0].name
  depends_on             = [kubernetes_namespace.monitoring]
}

resource "aws_iam_role_policy_attachment" "monitoring" {
  role       = module.irsa_monitoring.iam_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonPrometheusRemoteWriteAccess"
}

resource "helm_release" "kube-prometheus-stack" {
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  name       = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  version    = "43.1.1"
  values = [
    templatefile("./files/kube-prometheus-stack-values.yaml", {
      dns_zone                    = var.dns_zone
      environment                 = local.environment
      prometheus_irsa_arn         = module.irsa_monitoring.iam_role_arn
      prometheus_service_account  = "prometheus"
      prometheus_remote_write_url = var.prometheus_remote_write_url
    })
  ]
}

resource "helm_release" "prometheus-pushgateway" {
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  name       = "prometheus-pushgateway"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-pushgateway"
  version    = "2.0.2"
  values = [
    templatefile("./files/prometheus-pushgateway-values.yaml", {
      ingress_host    = "pushgateway.${var.dns_zone}"
      environment     = local.environment
      cluster_name    = local.name
      certificate_arn = module.acm.arn
    })
  ]
}

resource "helm_release" "prometheus-adapter" {
  count      = var.deploy_prometheus_adapter ? 1 : 0
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  name       = "prometheus-adapter"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-adapter"
  version    = "3.4.2"
  values = [
    templatefile("./files/prometheus-adapter-values.yaml", {
      prometheus_url  = "http://prometheus-server.prometheus.svc.cluster.local"
      prometheus_port = 80
    })
  ]
  depends_on = [
    module.eks
  ]
}

module "irsa_grafana" {
  source                 = "./modules/terraform-aws-eks-irsa"
  namespace              = kubernetes_namespace.monitoring.metadata[0].name
  name                   = "grafana"
  cluster_name           = module.eks.eks.id
  create_service_account = true
  service_account        = "grafana"
  description            = "Allows Grafana Pods to acess CloudWatch as DataSource"
}

resource "kubernetes_config_map" "grafana_dashboards" {
  metadata {
    name      = "grafana-dashboards"
    namespace = kubernetes_namespace.monitoring.metadata[0].name
  }

  data = {
    "app_resources.json"     = file("${path.module}/files/grafana-dashboards/app_resources.json")
    "cluster_metrics.json"   = file("${path.module}/files/grafana-dashboards/cluster_metrics.json")
    "global.json"            = file("${path.module}/files/grafana-dashboards/global.json")
    "jetstream_metrics.json" = file("${path.module}/files/grafana-dashboards/jetstream_metrics.json")
    "logs.json"              = file("${path.module}/files/grafana-dashboards/logs.json")
    "node_resources.json"    = file("${path.module}/files/grafana-dashboards/node_resources.json")
  }
}

resource "helm_release" "grafana" {
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  name       = "grafana"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "grafana"
  version    = "6.48.0"
  values = [
    templatefile("./files/grafana-values.yaml", {
      ingress_host         = "grafana.${var.dns_zone}",
      client_id            = var.google_oauth_client_id
      client_secret        = var.google_oauth_client_secret
      dashboards_configmap = kubernetes_config_map.grafana_dashboards.metadata[0].name
      storageclass_name    = var.default_storageclass
    })
  ]
  depends_on = [
    module.irsa_grafana,
    module.eks
  ]
}

module "s3_loki" {
  count            = var.deploy_loki ? 1 : 0
  source           = "./modules/terraform-aws-s3"
  name             = "eeek-${local.environment}-loki"
  full_bucket_name = "eeek-${local.environment}-loki"
}

resource "helm_release" "loki" {
  count      = var.deploy_loki ? 1 : 0
  namespace  = kubernetes_namespace.logging.metadata[0].name
  name       = "loki"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "loki"
  version    = "3.8.0"
  values = [
    templatefile("./files/loki-values.yaml", {
      aws_acm_arn    = module.acm.arn
      ingress_host   = "loki.${var.dns_zone}"
      s3_bucket_name = module.s3_loki[0].id
      cluster_name   = local.name
    })
  ]
}

resource "helm_release" "promtail" {
  namespace  = kubernetes_namespace.logging.metadata[0].name
  name       = "promtail"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "promtail"
  version    = "6.7.4"
  values = [
    file("./files/promtail-values.yaml")
  ]
}
